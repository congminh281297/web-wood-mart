import React from 'react';

const Header = () => {
  return (
    <header
      className="main-header header-has-no-bg header-shop icons-design-line header-color-dark header-mobile-center header-sticky-real"
      data-sticky-class="header-mobile-center header-color-dark"
    >
      <div className="container">
        <div className="wrapp-header" style={{ minHeight: '85px' }}>
          <div className="header-left-side">
            <div className="woodmart-burger-icon mobile-nav-icon">
              <span className="woodmart-burger" />
              <span
                className="woodmart-burger-label"
                data-translate="header.settings.menu"
              >
                Menu
              </span>
            </div>
          </div>
          <div
            className="
          site-navigation
          woodmart-navigation
          menu-left
          navigation-style-bordered
          main-nav
        "
          >
            <div className="menu-main-navigation-container">
              <div id="shopify-section-main-menu" className="shopify-section">
                <ul id="menu-main-navigation" className="menu">
                  <li
                    id="menu-item-1514566922536"
                    className="
                  menu-item menu-item-type-custom menu-item-home
                  item-level-0
                  menu-item-1514566922536 menu-item-no-children
                  with-offsets
                "
                  >
                    <a className="woodmart-nav-link">
                      <span>
                        <span className="lang1">Home</span>
                        <span className="lang2">Home</span>
                      </span>
                    </a>
                    <style
                      type="text/css"
                      dangerouslySetInnerHTML={{ __html: '' }}
                    />
                  </li>
                  <li
                    id="menu-item-1514566951009"
                    className="
                  menu-item
                  menu-item-type-post_type
                  menu-item-shop
                  menu-item-1514566951009
                  menu-item-design-full-width
                  menu-mega-dropdown
                  item-level-0 item-event-hover
                  menu-item-has-children
                  with-offsets
                "
                  >
                    <a
                      href="https://woodmart-minimalism.myshopify.com/collections/shop"
                      className="woodmart-nav-link"
                    >
                      <span>
                        <span className="lang1">Shop</span>
                        <span className="lang2">Shop</span>
                      </span>
                    </a>
                    <div
                      className="sub-menu-dropdown color-scheme-dark"
                      style={{ left: '-129.266px' }}
                    >
                      <div className="container">
                        <div className="vc_section vc_custom_1482224730326">
                          <div
                            className="
                          vc_row
                          wpb_row
                          vc_row-fluid vc_row-o-content-top vc_row-flex
                        "
                          >
                            <div
                              className="
                            wpb_column
                            vc_column_container vc_col-sm-2
                          "
                            >
                              <div className="vc_column-inner">
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li>
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">
                                            Shop pages
                                          </span>
                                          <span className="lang2">
                                            Shop pages
                                          </span>
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817236522">
                                            <span>
                                              <span className="lang1">
                                                Filters area
                                              </span>
                                              <span className="lang2">
                                                Filters area
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li
                                          className="
                                        item-with-label item-label-hot
                                      "
                                        >
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817728042">
                                            <span>
                                              <span className="lang1">
                                                Hidden sidebar
                                              </span>
                                              <span className="lang2">
                                                Hidden sidebar
                                              </span>
                                            </span>
                                            <span
                                              className="
                                            menu-label menu-label-hot
                                          "
                                            >
                                              HOT
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817367594">
                                            <span>
                                              <span className="lang1">
                                                No page heading
                                              </span>
                                              <span className="lang2">
                                                No page heading
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817760810">
                                            <span>
                                              <span className="lang1">
                                                Small categories menu
                                              </span>
                                              <span className="lang2">
                                                Small categories menu
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817334826">
                                            <span>
                                              <span className="lang1">
                                                Masonry grid
                                              </span>
                                              <span className="lang2">
                                                Masonry grid
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31757303850">
                                            <span>
                                              <span className="lang1">
                                                With background
                                              </span>
                                              <span className="lang2">
                                                With background
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/furniture/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Category description
                                              </span>
                                              <span className="lang2">
                                                Category description
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Only categories
                                              </span>
                                              <span className="lang2">
                                                Only categories
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-demo2.myshopify.com/?preview_theme_id=14221344825">
                                            <span>
                                              <span className="lang1">
                                                Header overlap
                                              </span>
                                              <span className="lang2">
                                                Header overlap
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Default shop
                                              </span>
                                              <span className="lang2">
                                                Default shop
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div
                              className="
                            wpb_column
                            vc_column_container vc_col-sm-2
                          "
                            >
                              <div className="vc_column-inner">
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li
                                      className="
                                    item-with-label item-label-effects
                                  "
                                    >
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">
                                            Product hovers
                                          </span>
                                          <span className="lang2">
                                            Product hovers
                                          </span>{' '}
                                        </span>
                                        <span
                                          className="
                                        menu-label menu-label-effects
                                      "
                                        >
                                          EFFECTS
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Summary on hover
                                              </span>
                                              <span className="lang2">
                                                Summary on hover
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817760810">
                                            <span>
                                              <span className="lang1">
                                                Icons on hover
                                              </span>
                                              <span className="lang2">
                                                Icons on hover
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817367594">
                                            <span>
                                              <span className="lang1">
                                                Icons &amp; Add to cart
                                              </span>
                                              <span className="lang2">
                                                Icons &amp; Add to cart
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817236522">
                                            <span>
                                              <span className="lang1">
                                                Full info on image
                                              </span>
                                              <span className="lang2">
                                                Full info on image
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817334826">
                                            <span>
                                              <span className="lang1">
                                                All info on hover
                                              </span>
                                              <span className="lang2">
                                                All info on hover
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817728042">
                                            <span>
                                              <span className="lang1">
                                                Button on image
                                              </span>
                                              <span className="lang2">
                                                Button on image
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817269290">
                                            <span>
                                              <span className="lang1">
                                                Standard button
                                              </span>
                                              <span className="lang2">
                                                Standard button
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817891882">
                                            <span>
                                              <span className="lang1">
                                                Quick shop
                                              </span>
                                              <span className="lang2">
                                                Quick shop
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/?preview_theme_id=31817891882">
                                            <span>
                                              <span className="lang1">
                                                Categories hover #1
                                              </span>
                                              <span className="lang2">
                                                Categories hover #1
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/?preview_theme_id=31817826346">
                                            <span>
                                              <span className="lang1">
                                                Categories hover #2
                                              </span>
                                              <span className="lang2">
                                                Categories hover #2
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div
                              className="
                            wpb_column
                            vc_column_container vc_col-sm-2
                          "
                            >
                              <div className="vc_column-inner">
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li
                                      className="
                                    item-with-label item-label-unlimited
                                  "
                                    >
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">
                                            Product pages
                                          </span>
                                          <span className="lang2">
                                            Product pages
                                          </span>{' '}
                                        </span>
                                        <span
                                          className="
                                        menu-label menu-label-unlimited
                                      "
                                        >
                                          UNLIMITED
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/henectus-tincidunt/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Default
                                              </span>
                                              <span className="lang2">
                                                Default
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31817990186">
                                            <span>
                                              <span className="lang1">
                                                Centered
                                              </span>
                                              <span className="lang2">
                                                Centered
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31817662506">
                                            <span>
                                              <span className="lang1">
                                                Sticky description
                                              </span>
                                              <span className="lang2">
                                                Sticky description
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31816712234">
                                            <span>
                                              <span className="lang1">
                                                With shadow
                                              </span>
                                              <span className="lang2">
                                                With shadow
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/penatibus-parturient-orci-morbi/?preview_theme_id=31817990186">
                                            <span>
                                              <span className="lang1">
                                                With background
                                              </span>
                                              <span className="lang2">
                                                With background
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li
                                          className="
                                        item-with-label item-label-new
                                      "
                                        >
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31817662506">
                                            <span>
                                              <span className="lang1">
                                                Accordion tabs
                                              </span>
                                              <span className="lang2">
                                                Accordion tabs
                                              </span>
                                            </span>
                                            <span
                                              className="
                                            menu-label menu-label-new
                                          "
                                            >
                                              NEW
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31817891882">
                                            <span>
                                              <span className="lang1">
                                                Accordion in content
                                              </span>
                                              <span className="lang2">
                                                Accordion in content
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31817269290">
                                            <span>
                                              <span className="lang1">
                                                With sidebar
                                              </span>
                                              <span className="lang2">
                                                With sidebar
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div
                              className="
                            wpb_column
                            vc_column_container vc_col-sm-2
                          "
                            >
                              <div className="vc_column-inner">
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li>
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">
                                            Product images
                                          </span>
                                          <span className="lang2">
                                            Product images
                                          </span>
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/accessories/products/interdum-elit-vestibulum">
                                            <span>
                                              <span className="lang1">
                                                Thumbnails left
                                              </span>
                                              <span className="lang2">
                                                Thumbnails left
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31755272234">
                                            <span>
                                              <span className="lang1">
                                                Thumbnails bottom
                                              </span>
                                              <span className="lang2">
                                                Thumbnails bottom
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31724929066">
                                            <span>
                                              <span className="lang1">
                                                Sticky images
                                              </span>
                                              <span className="lang2">
                                                Sticky images
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31736660010">
                                            <span>
                                              <span className="lang1">
                                                One column
                                              </span>
                                              <span className="lang2">
                                                One column
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31817826346">
                                            <span>
                                              <span className="lang1">
                                                Two columns
                                              </span>
                                              <span className="lang2">
                                                Two columns
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/henectus-tincidunt/?preview_theme_id=31817236522">
                                            <span>
                                              <span className="lang1">
                                                Combined grid
                                              </span>
                                              <span className="lang2">
                                                Combined grid
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Zoom image
                                              </span>
                                              <span className="lang2">
                                                Zoom image
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/henectus-tincidunt/?preview_theme_id=31817760810">
                                            <span>
                                              <span className="lang1">
                                                Images size - small
                                              </span>
                                              <span className="lang2">
                                                Images size - small
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31817367594">
                                            <span>
                                              <span className="lang1">
                                                Images size - large
                                              </span>
                                              <span className="lang2">
                                                Images size - large
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/smart-watches-wood-edition/?preview_theme_id=31817728042">
                                            <span>
                                              <span className="lang1">
                                                Without thumbnails
                                              </span>
                                              <span className="lang2">
                                                Without thumbnails
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div
                              className="
                            wpb_column
                            vc_column_container vc_col-sm-2
                          "
                            >
                              <div className="vc_column-inner">
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li>
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">Shopify</span>
                                          <span className="lang2">Shopify</span>
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/cooking/products/interdum-elit-vestibulum/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Simple product
                                              </span>
                                              <span className="lang2">
                                                Simple product
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/accessories/products/wooden-bow-tie-man/?preview_theme_id=31817334826">
                                            <span>
                                              <span className="lang1">
                                                Variable product
                                              </span>
                                              <span className="lang2">
                                                Variable product
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/eames-plastic-side-chair/?preview_theme_id=31817400362">
                                            <span>
                                              <span className="lang1">
                                                Variant image product
                                              </span>
                                              <span className="lang2">
                                                Variant image product
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/cart">
                                            <span>
                                              <span className="lang1">
                                                Shopping Cart
                                              </span>
                                              <span className="lang2">
                                                Shopping Cart
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/checkout">
                                            <span>
                                              <span className="lang1">
                                                Checkout
                                              </span>
                                              <span className="lang2">
                                                Checkout
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/account/login">
                                            <span>
                                              <span className="lang1">
                                                My account
                                              </span>
                                              <span className="lang2">
                                                My account
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/account/login">
                                            <span>
                                              <span className="lang1">
                                                Wishlist
                                              </span>
                                              <span className="lang2">
                                                Wishlist
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/404">
                                            <span>
                                              <span className="lang1">
                                                404 Not Found
                                              </span>
                                              <span className="lang2">
                                                404 Not Found
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div
                              className="
                            wpb_column
                            vc_column_container vc_col-sm-2
                          "
                            >
                              <div className="vc_column-inner">
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li className="item-with-label item-label-best">
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">
                                            Features
                                          </span>
                                          <span className="lang2">
                                            Features
                                          </span>{' '}
                                        </span>
                                        <span className="menu-label menu-label-best">
                                          BEST
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/furniture/products/eames-plastic-side-chair/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                360° product viewer
                                              </span>
                                              <span className="lang2">
                                                360° product viewer
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/lighting/products/scelerisque-lacus/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                With video
                                              </span>
                                              <span className="lang2">
                                                With video
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/products/penatibus-parturient-orci-morbi/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                With countdown timer
                                              </span>
                                              <span className="lang2">
                                                With countdown timer
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/furniture/products/facilisis-ligula-aliquet/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Variations swatches
                                              </span>
                                              <span className="lang2">
                                                Variations swatches
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li
                                          className="
                                        item-with-label item-label-new
                                      "
                                        >
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817662506">
                                            <span>
                                              <span className="lang1">
                                                Infinit scrolling
                                              </span>
                                              <span className="lang2">
                                                Infinit scrolling
                                              </span>
                                            </span>
                                            <span
                                              className="
                                            menu-label menu-label-new
                                          "
                                            >
                                              NEW
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817826346">
                                            <span>
                                              <span className="lang1">
                                                Load more button
                                              </span>
                                              <span className="lang2">
                                                Load more button
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/?preview_theme_id=31817433130">
                                            <span>
                                              <span className="lang1">
                                                Catalog mode
                                              </span>
                                              <span className="lang2">
                                                Catalog mode
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <style
                          type="text/css"
                          data-type="vc_shortcodes-custom-css"
                          dangerouslySetInnerHTML={{
                            __html:
                              '\n                            .vc_custom_1482224730326 {\n                              padding-top: 5px !important;\n                              padding-bottom: 5px !important;\n                            }\n                          ',
                          }}
                        />
                      </div>
                    </div>
                    <style
                      type="text/css"
                      dangerouslySetInnerHTML={{
                        __html:
                          '\n                        .menu-item-1514566951009 > .sub-menu-dropdown {\n                          min-height: 120px;\n                          width: 100%;\n                        }\n                      ',
                      }}
                    />
                  </li>
                  <li
                    id="menu-item-1515560575324"
                    className="
                  menu-item
                  menu-item-type-post_type
                  menu-item-blog
                  menu-item-1515560575324
                  menu-item-design-sized
                  menu-mega-dropdown
                  item-level-0 item-event-hover
                  menu-item-has-children
                  with-offsets
                "
                  >
                    <a
                      href="https://woodmart-minimalism.myshopify.com/blogs/news"
                      className="woodmart-nav-link"
                    >
                      <span>
                        <span className="lang1">Blog</span>
                        <span className="lang2">Blog</span>
                      </span>
                    </a>
                    <div
                      className="sub-menu-dropdown color-scheme-dark"
                      style={{}}
                    >
                      <div className="container">
                        <div className="vc_section vc_custom_1482224730326">
                          <div
                            className="
                          vc_row
                          wpb_row
                          vc_row-fluid
                          vc_row-o-equal-height
                          vc_row-o-content-top
                          vc_row-flex
                        "
                          >
                            <div
                              className="
                            rtl-blog-col1
                            wpb_column
                            vc_column_container vc_col-sm-3
                          "
                            >
                              <div
                                className="
                              vc_column-inner vc_custom_1501507744264
                            "
                              >
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li>
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">
                                            Blog types
                                          </span>
                                          <span className="lang2">
                                            Blog types
                                          </span>
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/?preview_theme_id=31817433130">
                                            <span>
                                              <span className="lang1">
                                                Alternative
                                              </span>
                                              <span className="lang2">
                                                Alternative
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/?preview_theme_id=31817400362">
                                            <span>
                                              <span className="lang1">
                                                Small images
                                              </span>
                                              <span className="lang2">
                                                Small images
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/?preview_theme_id=31817367594">
                                            <span>
                                              <span className="lang1">
                                                Blog chess
                                              </span>
                                              <span className="lang2">
                                                Blog chess
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/?preview_theme_id=31817990186">
                                            <span>
                                              <span className="lang1">
                                                Masonry grid
                                              </span>
                                              <span className="lang2">
                                                Masonry grid
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li
                                          className="
                                        item-with-label item-label-feature
                                      "
                                        >
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/?preview_theme_id=31817269290">
                                            <span>
                                              <span className="lang1">
                                                Infinit scrolling
                                              </span>
                                              <span className="lang2">
                                                Infinit scrolling
                                              </span>
                                            </span>
                                            <span
                                              className="
                                            menu-label menu-label-feature
                                          "
                                            >
                                              FEATURE
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/?preview_theme_id=31817236522">
                                            <span>
                                              <span className="lang1">
                                                With background
                                              </span>
                                              <span className="lang2">
                                                With background
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/?preview_theme_id=31817236522">
                                            <span>
                                              <span className="lang1">
                                                Blog flat
                                              </span>
                                              <span className="lang2">
                                                Blog flat
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/?preview_theme_id=31816712234">
                                            <span>
                                              <span className="lang1">
                                                Default flat
                                              </span>
                                              <span className="lang2">
                                                Default flat
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div
                              className="
                            rtl-blog-col2
                            wpb_column
                            vc_column_container vc_col-sm-3
                          "
                            >
                              <div
                                className="
                              vc_column-inner vc_custom_1501507744264
                            "
                              >
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li>
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">
                                            Single posts
                                          </span>
                                          <span className="lang2">
                                            Single posts
                                          </span>
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/green-interior-design-inspiration">
                                            <span>
                                              <span className="lang1">
                                                Post example #1
                                              </span>
                                              <span className="lang2">
                                                Post example #1
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/minimalist-design-furniture-2016">
                                            <span>
                                              <span className="lang1">
                                                Post example #2
                                              </span>
                                              <span className="lang2">
                                                Post example #2
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/design-trends/sweet-seat-functional-seat-for-it-folks">
                                            <span>
                                              <span className="lang1">
                                                Post example #3
                                              </span>
                                              <span className="lang2">
                                                Post example #3
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/reinterprets-the-classic-bookshelf">
                                            <span>
                                              <span className="lang1">
                                                Post example #4
                                              </span>
                                              <span className="lang2">
                                                Post example #4
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/creative-water-features-and-exterior">
                                            <span>
                                              <span className="lang1">
                                                Post example #5
                                              </span>
                                              <span className="lang2">
                                                Post example #5
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/furniture-that-explores-wood-as-a-material">
                                            <span>
                                              <span className="lang1">
                                                Post example #6
                                              </span>
                                              <span className="lang2">
                                                Post example #6
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/the-big-design-wall-likes-pictures">
                                            <span>
                                              <span className="lang1">
                                                Post example #7
                                              </span>
                                              <span className="lang2">
                                                Post example #7
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/blogs/news/new-home-decor-from-john-doerson">
                                            <span>
                                              <span className="lang1">
                                                Post example #8
                                              </span>
                                              <span className="lang2">
                                                Post example #8
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div
                              className="
                            wpb_column
                            vc_column_container
                            vc_col-sm-6
                            vc_col-has-fill
                          "
                            >
                              <div
                                className="
                              vc_column-inner vc_custom_1498552559778
                            "
                              >
                                <div className="spb_wrapper">
                                  <div
                                    className="
                                  spb_widgetised_column
                                  spb_content_element
                                "
                                  >
                                    <div
                                      className="
                                    woodmart-widget
                                    sidebar-widget
                                    woodmart-recent-posts
                                  "
                                    >
                                      <h5
                                        className="widget-title"
                                        data-translate="blogs.article.recent_posts"
                                      >
                                        Recent Posts
                                      </h5>
                                      <ul className="woodmart-recent-posts-list">
                                        <li>
                                          <a
                                            className="recent-posts-thumbnail"
                                            href="https://woodmart-minimalism.myshopify.com/blogs/news/seating-collection-inspiration"
                                          >
                                            <img
                                              className="
                                            attachment-large
                                            sp-post-image
                                          "
                                              src="./Woodmart Minimalism_files/blog-grid-9_105x84_crop_center.jpg"
                                              width={75}
                                            />
                                          </a>
                                          <div className="recent-posts-info">
                                            <h5 className="entry-title">
                                              <a href="https://woodmart-minimalism.myshopify.com/blogs/news/seating-collection-inspiration">
                                                <span className="lang1">
                                                  Seating collection inspiration
                                                </span>
                                                <span className="lang2">
                                                  Seating collection inspiration
                                                </span>
                                              </a>
                                            </h5>
                                            <time className="recent-posts-time">
                                              May 16, 2018
                                            </time>
                                            <a
                                              className="recent-posts-comment"
                                              href="https://woodmart-minimalism.myshopify.com/blogs/news/seating-collection-inspiration/comments"
                                              data-translate="blogs.comments.comments_with_count|count:0"
                                            >
                                              Counts
                                            </a>
                                          </div>
                                        </li>
                                        <li>
                                          <a
                                            className="recent-posts-thumbnail"
                                            href="https://woodmart-minimalism.myshopify.com/blogs/news/minimalist-design-furniture-2016"
                                          >
                                            <img
                                              className="
                                            attachment-large
                                            sp-post-image
                                          "
                                              src="./Woodmart Minimalism_files/blog-grid-1_105x84_crop_center.jpg"
                                              width={75}
                                            />
                                          </a>
                                          <div className="recent-posts-info">
                                            <h5 className="entry-title">
                                              <a href="https://woodmart-minimalism.myshopify.com/blogs/news/minimalist-design-furniture-2016">
                                                <span className="lang1">
                                                  Minimalist design furniture
                                                  2016
                                                </span>
                                                <span className="lang2">
                                                  Minimalist design furniture
                                                  2016
                                                </span>
                                              </a>
                                            </h5>
                                            <time className="recent-posts-time">
                                              May 16, 2018
                                            </time>
                                            <a
                                              className="recent-posts-comment"
                                              href="https://woodmart-minimalism.myshopify.com/blogs/news/minimalist-design-furniture-2016/comments"
                                              data-translate="blogs.comments.comments_with_count|count:0"
                                            >
                                              Counts
                                            </a>
                                          </div>
                                        </li>
                                        <li>
                                          <a
                                            className="recent-posts-thumbnail"
                                            href="https://woodmart-minimalism.myshopify.com/blogs/news/green-interior-design-inspiration"
                                          >
                                            <img
                                              className="
                                            attachment-large
                                            sp-post-image
                                          "
                                              src="./Woodmart Minimalism_files/blog-grid-7_105x84_crop_center.jpg"
                                              width={75}
                                            />
                                          </a>
                                          <div className="recent-posts-info">
                                            <h5 className="entry-title">
                                              <a href="https://woodmart-minimalism.myshopify.com/blogs/news/green-interior-design-inspiration">
                                                <span className="lang1">
                                                  Green interior design
                                                  inspiration
                                                </span>
                                                <span className="lang2">
                                                  Green interior design
                                                  inspiration
                                                </span>
                                              </a>
                                            </h5>
                                            <time className="recent-posts-time">
                                              May 16, 2018
                                            </time>
                                            <a
                                              className="recent-posts-comment"
                                              href="https://woodmart-minimalism.myshopify.com/blogs/news/green-interior-design-inspiration/comments"
                                              data-translate="blogs.comments.comments_with_count|count:0"
                                            >
                                              Counts
                                            </a>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <style
                          type="text/css"
                          data-type="vc_shortcodes-custom-css"
                          dangerouslySetInnerHTML={{
                            __html:
                              '\n                            .vc_custom_1482224730326 {\n                              padding-top: 5px !important;\n                              padding-bottom: 5px !important;\n                            }\n                            .vc_custom_1501507744264 {\n                              margin-right: -60px !important;\n                            }\n                            .vc_custom_1501507751857 {\n                              margin-right: -90px !important;\n                              padding-left: 60px !important;\n                            }\n                            .vc_custom_1498552559778 {\n                              margin-top: -35px !important;\n                              margin-right: -20px !important;\n                              margin-bottom: -5px !important;\n                              margin-left: 90px !important;\n                              border-left-width: 1px !important;\n                              padding-right: 30px !important;\n                              padding-left: 30px !important;\n                              background-color: rgba(0, 0, 0, 0.02) !important;\n                              *background-color: rgb(0, 0, 0) !important;\n                              border-left-color: rgba(0, 0, 0, 0.05) !important;\n                              border-left-style: solid !important;\n                            }\n                          ',
                          }}
                        />
                      </div>
                    </div>
                    <style
                      type="text/css"
                      dangerouslySetInnerHTML={{
                        __html:
                          '\n                        .menu-item-1515560575324 > .sub-menu-dropdown {\n                          min-height: 120px;\n                          width: 800px;\n                        }\n                      ',
                      }}
                    />
                  </li>
                  <li
                    id="menu-item-1519635009579"
                    className="
                  menu-item
                  menu-item-type-post_type
                  menu-item-pages
                  menu-item-1519635009579
                  menu-item-design-sized
                  menu-mega-dropdown
                  item-level-0 item-event-hover
                  menu-item-has-children
                  with-offsets
                "
                  >
                    <a
                      href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo"
                      className="woodmart-nav-link"
                    >
                      <span>
                        <span className="lang1">Pages</span>
                        <span className="lang2">Pages</span>
                      </span>
                    </a>
                    <div
                      className="sub-menu-dropdown color-scheme-dark"
                      style={{}}
                    >
                      <div className="container">
                        <div className="vc_section vc_custom_1482224730326">
                          <div
                            className="
                          vc_row
                          wpb_row
                          vc_row-fluid vc_row-o-content-top vc_row-flex
                        "
                          >
                            <div
                              className="
                            wpb_column
                            vc_column_container vc_col-sm-6
                          "
                            >
                              <div className="vc_column-inner">
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li>
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">Page</span>
                                          <span className="lang2">Page</span>
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/pages/faqs">
                                            <span>
                                              <span className="lang1">
                                                FaQs
                                              </span>
                                              <span className="lang2">
                                                FaQs
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/pages/about-me">
                                            <span>
                                              <span className="lang1">
                                                About Me
                                              </span>
                                              <span className="lang2">
                                                About Me
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/pages/contact-us">
                                            <span>
                                              <span className="lang1">
                                                Contact Us
                                              </span>
                                              <span className="lang2">
                                                Contact Us
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-minimalism.myshopify.com/pages/landing-page">
                                            <span>
                                              <span className="lang1">
                                                Landing Page
                                              </span>
                                              <span className="lang2">
                                                Landing Page
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/?preview_theme_id=31817760810">
                                            <span>
                                              <span className="lang1">
                                                Layout Boxed
                                              </span>
                                              <span className="lang2">
                                                Layout Boxed
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <div
                              className="
                            wpb_column
                            vc_column_container vc_col-sm-6
                          "
                            >
                              <div className="vc_column-inner">
                                <div className="wpb_wrapper">
                                  <ul className="sub-menu mega-menu-list">
                                    <li>
                                      <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                                        <span>
                                          <span className="lang1">Header</span>
                                          <span className="lang2">Header</span>
                                        </span>
                                      </a>
                                      <ul className="sub-sub-menu">
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817400362">
                                            <span>
                                              <span className="lang1">
                                                Advanced
                                              </span>
                                              <span className="lang2">
                                                Advanced
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817662506">
                                            <span>
                                              <span className="lang1">
                                                E-Commerce
                                              </span>
                                              <span className="lang2">
                                                E-Commerce
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=32546324522">
                                            <span>
                                              <span className="lang1">
                                                Header base
                                              </span>
                                              <span className="lang2">
                                                Header base
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817891882">
                                            <span>
                                              <span className="lang1">
                                                Simplified
                                              </span>
                                              <span className="lang2">
                                                Simplified
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817990186">
                                            <span>
                                              <span className="lang1">
                                                Double menu
                                              </span>
                                              <span className="lang2">
                                                Double menu
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817269290">
                                            <span>
                                              <span className="lang1">
                                                Logo center
                                              </span>
                                              <span className="lang2">
                                                Logo center
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31817760810">
                                            <span>
                                              <span className="lang1">
                                                With categories menu
                                              </span>
                                              <span className="lang2">
                                                With categories menu
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31816712234">
                                            <span>
                                              <span className="lang1">
                                                Menu in top bar
                                              </span>
                                              <span className="lang2">
                                                Menu in top bar
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-default.myshopify.com/collections/shop/?preview_theme_id=31826444330">
                                            <span>
                                              <span className="lang1">
                                                Dark header
                                              </span>
                                              <span className="lang2">
                                                Dark header
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="https://woodmart-demo2.myshopify.com/?preview_theme_id=16496656441">
                                            <span>
                                              <span className="lang1">
                                                Colored header
                                              </span>
                                              <span className="lang2">
                                                Colored header
                                              </span>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <style
                          type="text/css"
                          data-type="vc_shortcodes-custom-css"
                          dangerouslySetInnerHTML={{
                            __html:
                              '\n                            .vc_custom_1482224730326 {\n                              padding-top: 5px !important;\n                              padding-bottom: 5px !important;\n                            }\n                          ',
                          }}
                        />
                      </div>
                    </div>
                    <style
                      type="text/css"
                      dangerouslySetInnerHTML={{
                        __html:
                          '\n                        .menu-item-1519635009579 > .sub-menu-dropdown {\n                          min-height: 120px;\n                          width: 400px;\n                        }\n                      ',
                      }}
                    />
                  </li>
                  <li
                    id="menu-item-1519634982521"
                    className="
                  menu-item menu-item-type-custom menu-item-buy
                  item-level-0
                  menu-item-1519634982521 menu-item-no-children
                  with-offsets
                "
                  >
                    <a
                      href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo"
                      className="woodmart-nav-link"
                    >
                      <span>
                        <span className="lang1">Buy</span>
                        <span className="lang2">Buy</span>
                      </span>
                    </a>
                    <style
                      type="text/css"
                      dangerouslySetInnerHTML={{ __html: '' }}
                    />
                  </li>
                </ul>
                <style
                  type="text/css"
                  dangerouslySetInnerHTML={{
                    __html:
                      '\n                    .menu-label.menu-label-hot {\n                      background-color: #83b735;\n                    }\n                    .menu-label.menu-label-hot:before {\n                      border-color: #83b735;\n                    }\n                    .menu-label.menu-label-effects {\n                      background-color: #fbbc34;\n                    }\n                    .menu-label.menu-label-effects:before {\n                      border-color: #fbbc34;\n                    }\n                    .menu-label.menu-label-unlimited {\n                      background-color: #d41212;\n                    }\n                    .menu-label.menu-label-unlimited:before {\n                      border-color: #d41212;\n                    }\n                    .menu-label.menu-label-best {\n                      background-color: #d41212;\n                    }\n                    .menu-label.menu-label-best:before {\n                      border-color: #d41212;\n                    }\n                    .menu-label.menu-label-feature {\n                      background-color: #d41212;\n                    }\n                    .menu-label.menu-label-feature:before {\n                      border-color: #d41212;\n                    }\n                  ',
                  }}
                />
              </div>
            </div>
          </div>
          <div className="site-logo">
            <div className="woodmart-logo-wrap">
              <a
                href="https://woodmart-minimalism.myshopify.com/"
                className="woodmart-logo woodmart-main-logo"
                rel="home"
              >
                <img
                  className="logo"
                  src="./Woodmart Minimalism_files/wood-logo-dark.svg"
                  style={{ maxWidth: '209px' }}
                  alt="Woodmart Minimalism"
                />
              </a>
            </div>
          </div>
          <div className="right-column">
            <div className="woodmart-header-links">
              <ul>
                <li className="item-level-0 my-account-with-text menu-item-register">
                  <a href="https://woodmart-minimalism.myshopify.com/account/login">
                    <span data-translate="header.settings.register">
                      Login / Register
                    </span>
                  </a>
                </li>
              </ul>
            </div>
            <div className="search-button woodmart-search-full-screen">
              <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#" />
              <div className="woodmart-search-wrapper">
                <div className="woodmart-search-inner">
                  <span className="woodmart-close-search">close</span>
                  <form
                    role="search"
                    method="get"
                    className="searchform woodmart-ajax-search"
                    action="https://woodmart-minimalism.myshopify.com/search"
                    data-thumbnail={1}
                    data-price={1}
                    data-post-type="product"
                    data-count={15}
                  >
                    <div>
                      <label className="screen-reader-text" htmlFor="q" />
                      <input
                        type="text"
                        className="s"
                        data-translate="general.search.placeholder"
                        placeholder="Search for products"
                        defaultValue
                        name="q"
                        autoComplete="off"
                        style={{ paddingRight: '65px' }}
                      />
                      <input
                        type="hidden"
                        name="post_type"
                        defaultValue="product"
                      />
                      <button
                        type="submit"
                        className="searchsubmit"
                        data-translate="general.search.submit"
                      >
                        Search
                      </button>
                    </div>
                  </form>
                  <div className="search-info-text">
                    <span data-translate="general.search.info_search">
                      Start typing to see products you are looking for.
                    </span>
                  </div>
                  <div className="search-results-wrapper">
                    <div className="woodmart-scroll has-scrollbar">
                      <div
                        className="
                      woodmart-search-results woodmart-scroll-content
                    "
                        tabIndex={0}
                        style={{ right: '-17px' }}
                      >
                        <div
                          className="autocomplete-suggestions"
                          style={{
                            position: 'absolute',
                            maxHeight: '300px',
                            zIndex: 9999,
                            width: '308px',
                            display: 'none',
                          }}
                        />
                      </div>
                      <div
                        className="woodmart-scroll-pane"
                        style={{ display: 'none' }}
                      >
                        <div
                          className="woodmart-scroll-slider"
                          style={{
                            height: '752px',
                            transform: 'translate(0px, 0px)',
                          }}
                        />
                      </div>
                    </div>
                    <div className="woodmart-search-loader" />
                  </div>
                </div>
              </div>
            </div>
            <div className="woodmart-wishlist-info-widget">
              <a href="https://woodmart-minimalism.myshopify.com/?fbclid=IwAR2kUQGWTOGovw32ZC5e9ZSvvnjzashoPz04Ht6x8DV_eueDUuteP7O3wPo#">
                <span className="wishlist-info-wrap">
                  <span
                    className="wishlist-label"
                    data-translate="header.settings.wishlist"
                  >
                    Wishlist
                  </span>
                  <span className="count">0</span>
                </span>
              </a>
            </div>
            <div
              className="
            woodmart-shopping-cart
            woodmart-cart-design-1
            woodmart-cart-icon
            cart-widget-opener
          "
            >
              <a href="https://woodmart-minimalism.myshopify.com/cart">
                <span className="woodmart-cart-totals">
                  <span className="woodmart-cart-number">
                    0
                    <span data-translate="cart.header.total_numb">item(s)</span>
                  </span>
                  <span className="subtotal-divider">/</span>
                  <span className="woodmart-cart-subtotal">
                    <span className="shopify-Price-amount amount">
                      <span className="money" data-currency-usd="$0.00">
                        $0.00
                      </span>
                    </span>
                  </span>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
