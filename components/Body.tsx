import React from 'react';

const Body = () => {
  return (
    <div className="main-page-wrapper">
      <div className="container">
        <div className="row content-layout-wrapper">
          <div className="site-content col-sm-12">
            {/* /templates/index.liquid */}
            {/* BEGIN content_for_index */}
            <div id="shopify-section-1516961703515" className="shopify-section">
              <div
                data-section-id={1516961703515}
                data-section-type="slideshow-section"
              >
                <div
                  data-vc-full-width="true"
                  data-vc-full-width-init="true"
                  data-vc-stretch-content="true"
                  className="vc_row wpb_row vc_row-fluid vc_custom_1516961703515 vc_row-has-fill woodmart-bg-center-center vc_row-no-padding woodmart-bg-left-top"
                  style={{
                    marginTop: '-40px',
                    marginBottom: '20px',
                    padding: '0px 30px',
                    position: 'relative',
                    left: '-31.5938px',
                    boxSizing: 'border-box',
                    width: '1264px',
                  }}
                >
                  <div className="wpb_column vc_column_container vc_col-sm-12 ">
                    <div className="vc_column-inner vc_custom_1496220809434">
                      <div className="wpb_wrapper">
                        <div className="slideshow-background slideshow-section">
                          <div
                            className="data-slideshow"
                            data-auto={5000}
                            data-paging="true"
                            data-nav="false"
                            data-transition="fade"
                            data-loop="true"
                            data-dots="true"
                            style={{ display: 'none' }}
                          />
                          <div
                            id="slideshow-section-1516961703515"
                            className="slideshow owl-carousel owl-loaded owl-drag"
                          >
                            <div className="owl-stage-outer">
                              <div
                                className="owl-stage"
                                style={{
                                  transform: 'translate3d(-4816px, 0px, 0px)',
                                  transition: 'all 0s ease 0s',
                                  width: '8428px',
                                }}
                              >
                                <div
                                  className="owl-item cloned"
                                  style={{ width: '1204px' }}
                                >
                                  <div
                                    className="item"
                                    style={{
                                      background:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/files/mini-slide-2.jpg?v=1525974146) center center no-repeat',
                                      backgroundSize: 'cover',
                                    }}
                                    data-dot="<span>02</span>"
                                  >
                                    <div className="slide-inner">
                                      <div
                                        className="container"
                                        style={{ position: 'relative' }}
                                      >
                                        <img src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/bg_transparent2.png?v=1525974317" />
                                        <div className="content_slideshow content-1516961703515-1">
                                          <div className="row">
                                            <div className="col-sm-0">
                                              <div className="vc_column-inner vc_custom_1497367543743">
                                                <div className="wpb_wrapper" />
                                              </div>
                                            </div>
                                            <div className="col-sm-12 col-lg-12 col-md-12">
                                              <div className="vc_column-inner vc_custom_1496220809434">
                                                <div
                                                  className="wpb_wrapper"
                                                  style={{ textAlign: 'left' }}
                                                >
                                                  <h4 style={{ color: '#333' }}>
                                                    <span className="lang1">
                                                      The Minimal
                                                      <br /> Solutions Trend
                                                    </span>
                                                    <span className="lang2">
                                                      The Minimal
                                                      <br /> Solutions Trend
                                                    </span>
                                                  </h4>
                                                  <p
                                                    className="content1"
                                                    style={{ color: '#797979' }}
                                                  >
                                                    <span className="lang1">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                    <span className="lang2">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                  </p>
                                                  <a
                                                    href
                                                    className="btn btn-style-default btn-color-black btn-size-default"
                                                  >
                                                    <span className="lang1">
                                                      Shop Now
                                                    </span>
                                                    <span className="lang2">
                                                      Shop Now
                                                    </span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <style
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              '\n                      .vc_custom_1516961703515 .slideshow .content-1516961703515-1 {\n                        top:18%;\n                        left:3%;\n                      }\n                      @media(min-width:768px){\n                        .vc_custom_1516961703515 .slideshow .content-1516961703515-1 {\n                          left:4%;\n                        }\n                      }\n                    ',
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="owl-item cloned"
                                  style={{ width: '1204px' }}
                                >
                                  <div
                                    className="item"
                                    style={{
                                      background:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/files/mini-slide-3.jpg?v=1525974146) center center no-repeat',
                                      backgroundSize: 'cover',
                                    }}
                                    data-dot="<span>03</span>"
                                  >
                                    <div className="slide-inner">
                                      <div
                                        className="container"
                                        style={{ position: 'relative' }}
                                      >
                                        <img src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/bg_transparent2.png?v=1525974317" />
                                        <div className="content_slideshow content-1517027632004">
                                          <div className="row">
                                            <div className="col-sm-0">
                                              <div className="vc_column-inner vc_custom_1497367543743">
                                                <div className="wpb_wrapper" />
                                              </div>
                                            </div>
                                            <div className="col-sm-12 col-lg-12 col-md-12">
                                              <div className="vc_column-inner vc_custom_1496220809434">
                                                <div
                                                  className="wpb_wrapper"
                                                  style={{ textAlign: 'left' }}
                                                >
                                                  <h4 style={{ color: '#333' }}>
                                                    <span className="lang1">
                                                      Watches Accuracy
                                                      <br /> &amp; Performance
                                                    </span>
                                                    <span className="lang2">
                                                      Watches Accuracy
                                                      <br /> &amp; Performance
                                                    </span>
                                                  </h4>
                                                  <p
                                                    className="content1"
                                                    style={{ color: '#797979' }}
                                                  >
                                                    <span className="lang1">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                    <span className="lang2">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                  </p>
                                                  <a
                                                    href
                                                    className="btn btn-style-default btn-color-black btn-size-default"
                                                  >
                                                    <span className="lang1">
                                                      Shop Now
                                                    </span>
                                                    <span className="lang2">
                                                      Shop Now
                                                    </span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <style
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              '\n                      .vc_custom_1516961703515 .slideshow .content-1517027632004 {\n                        top:18%;\n                        left:3%;\n                      }\n                      @media(min-width:768px){\n                        .vc_custom_1516961703515 .slideshow .content-1517027632004 {\n                          left:4%;\n                        }\n                      }\n                    ',
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="owl-item"
                                  style={{ width: '1204px' }}
                                >
                                  <div
                                    className="item"
                                    style={{
                                      background:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/files/mini-slide-1.jpg?v=1525974146) center center no-repeat',
                                      backgroundSize: 'cover',
                                    }}
                                    data-dot="<span>01</span>"
                                  >
                                    <div className="slide-inner">
                                      <div
                                        className="container"
                                        style={{ position: 'relative' }}
                                      >
                                        <img src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/bg_transparent2.png?v=1525974317" />
                                        <div className="content_slideshow content-1516961703515-0">
                                          <div className="row">
                                            <div className="col-sm-0">
                                              <div className="vc_column-inner vc_custom_1497367543743">
                                                <div className="wpb_wrapper" />
                                              </div>
                                            </div>
                                            <div className="col-sm-12 col-lg-12 col-md-12">
                                              <div className="vc_column-inner vc_custom_1496220809434">
                                                <div
                                                  className="wpb_wrapper"
                                                  style={{ textAlign: 'left' }}
                                                >
                                                  <h4 style={{ color: '#333' }}>
                                                    <span className="lang1">
                                                      WoodMart
                                                      <br /> Minimalist 2018
                                                    </span>
                                                    <span className="lang2">
                                                      WoodMart
                                                      <br /> Minimalist 2018
                                                    </span>
                                                  </h4>
                                                  <p
                                                    className="content1"
                                                    style={{ color: '#797979' }}
                                                  >
                                                    <span className="lang1">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                    <span className="lang2">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                  </p>
                                                  <a
                                                    href
                                                    className="btn btn-style-default btn-color-black btn-size-default"
                                                  >
                                                    <span className="lang1">
                                                      Shop Now
                                                    </span>
                                                    <span className="lang2">
                                                      Shop Now
                                                    </span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <style
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              '\n                      .vc_custom_1516961703515 .slideshow .content-1516961703515-0 {\n                        top:18%;\n                        left:3%;\n                      }\n                      @media(min-width:768px){\n                        .vc_custom_1516961703515 .slideshow .content-1516961703515-0 {\n                          left:4%;\n                        }\n                      }\n                    ',
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="owl-item"
                                  style={{ width: '1204px' }}
                                >
                                  <div
                                    className="item"
                                    style={{
                                      background:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/files/mini-slide-2.jpg?v=1525974146) center center no-repeat',
                                      backgroundSize: 'cover',
                                    }}
                                    data-dot="<span>02</span>"
                                  >
                                    <div className="slide-inner">
                                      <div
                                        className="container"
                                        style={{ position: 'relative' }}
                                      >
                                        <img src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/bg_transparent2.png?v=1525974317" />
                                        <div className="content_slideshow content-1516961703515-1">
                                          <div className="row">
                                            <div className="col-sm-0">
                                              <div className="vc_column-inner vc_custom_1497367543743">
                                                <div className="wpb_wrapper" />
                                              </div>
                                            </div>
                                            <div className="col-sm-12 col-lg-12 col-md-12">
                                              <div className="vc_column-inner vc_custom_1496220809434">
                                                <div
                                                  className="wpb_wrapper"
                                                  style={{ textAlign: 'left' }}
                                                >
                                                  <h4 style={{ color: '#333' }}>
                                                    <span className="lang1">
                                                      The Minimal
                                                      <br /> Solutions Trend
                                                    </span>
                                                    <span className="lang2">
                                                      The Minimal
                                                      <br /> Solutions Trend
                                                    </span>
                                                  </h4>
                                                  <p
                                                    className="content1"
                                                    style={{ color: '#797979' }}
                                                  >
                                                    <span className="lang1">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                    <span className="lang2">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                  </p>
                                                  <a
                                                    href
                                                    className="btn btn-style-default btn-color-black btn-size-default"
                                                  >
                                                    <span className="lang1">
                                                      Shop Now
                                                    </span>
                                                    <span className="lang2">
                                                      Shop Now
                                                    </span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <style
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              '\n                      .vc_custom_1516961703515 .slideshow .content-1516961703515-1 {\n                        top:18%;\n                        left:3%;\n                      }\n                      @media(min-width:768px){\n                        .vc_custom_1516961703515 .slideshow .content-1516961703515-1 {\n                          left:4%;\n                        }\n                      }\n                    ',
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="owl-item active"
                                  style={{ width: '1204px' }}
                                >
                                  <div
                                    className="item"
                                    style={{
                                      background:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/files/mini-slide-3.jpg?v=1525974146) center center no-repeat',
                                      backgroundSize: 'cover',
                                    }}
                                    data-dot="<span>03</span>"
                                  >
                                    <div className="slide-inner">
                                      <div
                                        className="container"
                                        style={{ position: 'relative' }}
                                      >
                                        <img src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/bg_transparent2.png?v=1525974317" />
                                        <div className="content_slideshow content-1517027632004">
                                          <div className="row">
                                            <div className="col-sm-0">
                                              <div className="vc_column-inner vc_custom_1497367543743">
                                                <div className="wpb_wrapper" />
                                              </div>
                                            </div>
                                            <div className="col-sm-12 col-lg-12 col-md-12">
                                              <div className="vc_column-inner vc_custom_1496220809434">
                                                <div
                                                  className="wpb_wrapper"
                                                  style={{ textAlign: 'left' }}
                                                >
                                                  <h4 style={{ color: '#333' }}>
                                                    <span className="lang1">
                                                      Watches Accuracy
                                                      <br /> &amp; Performance
                                                    </span>
                                                    <span className="lang2">
                                                      Watches Accuracy
                                                      <br /> &amp; Performance
                                                    </span>
                                                  </h4>
                                                  <p
                                                    className="content1"
                                                    style={{ color: '#797979' }}
                                                  >
                                                    <span className="lang1">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                    <span className="lang2">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                  </p>
                                                  <a
                                                    href
                                                    className="btn btn-style-default btn-color-black btn-size-default"
                                                  >
                                                    <span className="lang1">
                                                      Shop Now
                                                    </span>
                                                    <span className="lang2">
                                                      Shop Now
                                                    </span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <style
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              '\n                      .vc_custom_1516961703515 .slideshow .content-1517027632004 {\n                        top:18%;\n                        left:3%;\n                      }\n                      @media(min-width:768px){\n                        .vc_custom_1516961703515 .slideshow .content-1517027632004 {\n                          left:4%;\n                        }\n                      }\n                    ',
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="owl-item cloned"
                                  style={{ width: '1204px' }}
                                >
                                  <div
                                    className="item"
                                    style={{
                                      background:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/files/mini-slide-1.jpg?v=1525974146) center center no-repeat',
                                      backgroundSize: 'cover',
                                    }}
                                    data-dot="<span>01</span>"
                                  >
                                    <div className="slide-inner">
                                      <div
                                        className="container"
                                        style={{ position: 'relative' }}
                                      >
                                        <img src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/bg_transparent2.png?v=1525974317" />
                                        <div className="content_slideshow content-1516961703515-0">
                                          <div className="row">
                                            <div className="col-sm-0">
                                              <div className="vc_column-inner vc_custom_1497367543743">
                                                <div className="wpb_wrapper" />
                                              </div>
                                            </div>
                                            <div className="col-sm-12 col-lg-12 col-md-12">
                                              <div className="vc_column-inner vc_custom_1496220809434">
                                                <div
                                                  className="wpb_wrapper"
                                                  style={{ textAlign: 'left' }}
                                                >
                                                  <h4 style={{ color: '#333' }}>
                                                    <span className="lang1">
                                                      WoodMart
                                                      <br /> Minimalist 2018
                                                    </span>
                                                    <span className="lang2">
                                                      WoodMart
                                                      <br /> Minimalist 2018
                                                    </span>
                                                  </h4>
                                                  <p
                                                    className="content1"
                                                    style={{ color: '#797979' }}
                                                  >
                                                    <span className="lang1">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                    <span className="lang2">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                  </p>
                                                  <a
                                                    href
                                                    className="btn btn-style-default btn-color-black btn-size-default"
                                                  >
                                                    <span className="lang1">
                                                      Shop Now
                                                    </span>
                                                    <span className="lang2">
                                                      Shop Now
                                                    </span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <style
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              '\n                      .vc_custom_1516961703515 .slideshow .content-1516961703515-0 {\n                        top:18%;\n                        left:3%;\n                      }\n                      @media(min-width:768px){\n                        .vc_custom_1516961703515 .slideshow .content-1516961703515-0 {\n                          left:4%;\n                        }\n                      }\n                    ',
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="owl-item cloned"
                                  style={{ width: '1204px' }}
                                >
                                  <div
                                    className="item"
                                    style={{
                                      background:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/files/mini-slide-2.jpg?v=1525974146) center center no-repeat',
                                      backgroundSize: 'cover',
                                    }}
                                    data-dot="<span>02</span>"
                                  >
                                    <div className="slide-inner">
                                      <div
                                        className="container"
                                        style={{ position: 'relative' }}
                                      >
                                        <img src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/bg_transparent2.png?v=1525974317" />
                                        <div className="content_slideshow content-1516961703515-1">
                                          <div className="row">
                                            <div className="col-sm-0">
                                              <div className="vc_column-inner vc_custom_1497367543743">
                                                <div className="wpb_wrapper" />
                                              </div>
                                            </div>
                                            <div className="col-sm-12 col-lg-12 col-md-12">
                                              <div className="vc_column-inner vc_custom_1496220809434">
                                                <div
                                                  className="wpb_wrapper"
                                                  style={{ textAlign: 'left' }}
                                                >
                                                  <h4 style={{ color: '#333' }}>
                                                    <span className="lang1">
                                                      The Minimal
                                                      <br /> Solutions Trend
                                                    </span>
                                                    <span className="lang2">
                                                      The Minimal
                                                      <br /> Solutions Trend
                                                    </span>
                                                  </h4>
                                                  <p
                                                    className="content1"
                                                    style={{ color: '#797979' }}
                                                  >
                                                    <span className="lang1">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                    <span className="lang2">
                                                      Facilisi a consequat
                                                      montes sodales inceptos
                                                      dignissim
                                                      <br />
                                                      aliquet ac aenean lacus
                                                      fames parturien sociis
                                                      magna cras
                                                      <br />
                                                      nibh phasellus maecenas
                                                      fames aliquet parturien
                                                      odio.
                                                    </span>
                                                  </p>
                                                  <a
                                                    href
                                                    className="btn btn-style-default btn-color-black btn-size-default"
                                                  >
                                                    <span className="lang1">
                                                      Shop Now
                                                    </span>
                                                    <span className="lang2">
                                                      Shop Now
                                                    </span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <style
                                          dangerouslySetInnerHTML={{
                                            __html:
                                              '\n                      .vc_custom_1516961703515 .slideshow .content-1516961703515-1 {\n                        top:18%;\n                        left:3%;\n                      }\n                      @media(min-width:768px){\n                        .vc_custom_1516961703515 .slideshow .content-1516961703515-1 {\n                          left:4%;\n                        }\n                      }\n                    ',
                                          }}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="owl-nav disabled">
                              <div className="owl-prev" />
                              <div className="owl-next" />
                            </div>
                            <div className="owl-dots">
                              <div className="owl-dot">
                                <span>01</span>
                              </div>
                              <div className="owl-dot">
                                <span>02</span>
                              </div>
                              <div className="owl-dot active">
                                <span>03</span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="vc_row-full-width vc_clearfix" />
                <style
                  dangerouslySetInnerHTML={{
                    __html:
                      '\n    .vc_custom_1496220809434 {\n      padding-top: 0px !important;\n    }\n  ',
                  }}
                />
              </div>
            </div>
            <div id="shopify-section-1517801842869" className="shopify-section">
              <div
                data-section-id={1517801842869}
                data-section-type="carousel-section"
              >
                <div
                  data-vc-full-width="true"
                  data-vc-full-width-init="true"
                  data-vc-stretch-content="true"
                  className="vc_row wpb_row vc_row-fluid vc_custom_1517801842869"
                  style={{
                    marginBottom: '9px',
                    padding: '0px 15px',
                    position: 'relative',
                    left: '-31.5938px',
                    boxSizing: 'border-box',
                    width: '1264px',
                  }}
                >
                  <div className="wpb_column vc_column_container vc_col-sm-12">
                    <div className="vc_column-inner">
                      <div className="wpb_wrapper">
                        <div
                          id="slide_1517801842869"
                          className="banners-carousel-wrapper banners-spacing-20 woodmart-spacing-20"
                        >
                          <div
                            className="data-carousel"
                            data-auto={5000}
                            data-items={3}
                            data-1200={3}
                            data-992={3}
                            data-768={2}
                            data-480={2}
                            data-320={2}
                            data-paging="false"
                            data-nav="false"
                            data-loop="true"
                            data-margin={0}
                            data-prev
                            data-next
                            style={{ display: 'none' }}
                          />
                          <div className="owl-carousel banners-carousel owl-loaded owl-drag">
                            <div className="owl-stage-outer">
                              <div
                                className="owl-stage"
                                style={{
                                  transform: 'translate3d(0px, 0px, 0px)',
                                  transition: 'all 0s ease 0s',
                                  width: '1215px',
                                }}
                              >
                                <div
                                  className="owl-item active"
                                  style={{ width: '404.667px' }}
                                >
                                  <div className="promo-banner-wrapper">
                                    <div className="promo-banner cursor-pointer banner-vr-align-middle banner-hr-align-left banner- banner-hover-zoom color-scheme-dark banner-btn-size-small banner-btn-style-link banner-increased-padding with-btn banner-btn-position-hover">
                                      <div className="main-wrapp-img">
                                        <div className="banner-image">
                                          <img
                                            src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/banner_minimalism3.jpg?v=1525974256"
                                            alt=""
                                            className="promo-banner-image image-1 attachment-full"
                                          />
                                        </div>
                                      </div>
                                      <div className="wrapper-content-banner">
                                        <div className="content-banner text-left content-width-100 content-spacing-default">
                                          <div className="banner-title-wrap banner-title-default">
                                            <span className="banner-subtitle subtitle-color-default subtitle-style-default">
                                              <span className="lang1">
                                                Hitech Innovations
                                              </span>
                                              <span className="lang2">
                                                Hitech Innovations
                                              </span>
                                            </span>
                                            <h4 className="banner-title woodmart-font-weight-600">
                                              <span className="lang1">
                                                Smart Watches
                                              </span>
                                              <span className="lang2">
                                                Smart Watches
                                              </span>
                                            </h4>
                                          </div>
                                          <div className="banner-btn-wrapper">
                                            <div className="woodmart-button-wrapper text-left">
                                              <a
                                                href
                                                title
                                                className="btn btn-color-primary btn-style-link btn-size-small"
                                              >
                                                <span className="lang1">
                                                  Read More
                                                </span>
                                                <span className="lang2">
                                                  Read More
                                                </span>
                                              </a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="owl-item active"
                                  style={{ width: '404.667px' }}
                                >
                                  <div className="promo-banner-wrapper">
                                    <div className="promo-banner cursor-pointer banner-vr-align-middle banner-hr-align-left banner- banner-hover-zoom color-scheme-dark banner-btn-size-small banner-btn-style-link banner-increased-padding with-btn banner-btn-position-hover">
                                      <div className="main-wrapp-img">
                                        <div className="banner-image">
                                          <img
                                            src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/banner_minimalism2.jpg?v=1525974256"
                                            alt=""
                                            className="promo-banner-image image-1 attachment-full"
                                          />
                                        </div>
                                      </div>
                                      <div className="wrapper-content-banner">
                                        <div className="content-banner text-left content-width-100 content-spacing-default">
                                          <div className="banner-title-wrap banner-title-default">
                                            <span className="banner-subtitle subtitle-color-default subtitle-style-default">
                                              <span className="lang1">
                                                Premium Leather
                                              </span>
                                              <span className="lang2">
                                                Premium Leather
                                              </span>
                                            </span>
                                            <h4 className="banner-title woodmart-font-weight-600">
                                              <span className="lang1">
                                                Smart Shoes
                                              </span>
                                              <span className="lang2">
                                                Smart Shoes
                                              </span>
                                            </h4>
                                          </div>
                                          <div className="banner-btn-wrapper">
                                            <div className="woodmart-button-wrapper text-left">
                                              <a
                                                href
                                                title
                                                className="btn btn-color-primary btn-style-link btn-size-small"
                                              >
                                                <span className="lang1">
                                                  Read More
                                                </span>
                                                <span className="lang2">
                                                  Read More
                                                </span>
                                              </a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div
                                  className="owl-item active"
                                  style={{ width: '404.667px' }}
                                >
                                  <div className="promo-banner-wrapper">
                                    <div className="promo-banner cursor-pointer banner-vr-align-middle banner-hr-align-left banner- banner-hover-zoom color-scheme-dark banner-btn-size-small banner-btn-style-link banner-increased-padding with-btn banner-btn-position-hover">
                                      <div className="main-wrapp-img">
                                        <div className="banner-image">
                                          <img
                                            src="//cdn.shopify.com/s/files/1/0002/7410/4364/files/banner_minimalism1-1.jpg?v=1525974257"
                                            alt=""
                                            className="promo-banner-image image-1 attachment-full"
                                          />
                                        </div>
                                      </div>
                                      <div className="wrapper-content-banner">
                                        <div className="content-banner text-left content-width-100 content-spacing-default">
                                          <div className="banner-title-wrap banner-title-default">
                                            <span className="banner-subtitle subtitle-color-default subtitle-style-default">
                                              <span className="lang1">
                                                Sound Explosion
                                              </span>
                                              <span className="lang2">
                                                Sound Explosion
                                              </span>
                                            </span>
                                            <h4 className="banner-title woodmart-font-weight-600">
                                              <span className="lang1">
                                                Smart Electronic
                                              </span>
                                              <span className="lang2">
                                                Smart Electronic
                                              </span>
                                            </h4>
                                          </div>
                                          <div className="banner-btn-wrapper">
                                            <div className="woodmart-button-wrapper text-left">
                                              <a
                                                href
                                                title
                                                className="btn btn-color-primary btn-style-link btn-size-small"
                                              >
                                                <span className="lang1">
                                                  Read More
                                                </span>
                                                <span className="lang2">
                                                  Read More
                                                </span>
                                              </a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="owl-nav disabled">
                              <div className="owl-prev" />
                              <div className="owl-next" />
                            </div>
                            <div className="owl-dots disabled" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="vc_row-full-width vc_clearfix" />
                <style
                  dangerouslySetInnerHTML={{
                    __html:
                      '\n    .vc_custom_1517801842869 { \n      border-top-width: 0px !important;background-color: transparent;\n    }.vc_custom_1517801842869 .vc_column_container>.vc_column-inner { \n      padding-left: 20px;\n      padding-right: 20px; \n    }',
                  }}
                />
              </div>
            </div>
            <div id="shopify-section-1517802175241" className="shopify-section">
              <div
                className="product-grid"
                data-section-id={1517802175241}
                data-section-type="product-grid-section"
              >
                <div
                  id="product-grid-1517802175241"
                  className="vc_row wpb_row vc_row-fluid vc_custom_1517802175241 vc_row-has-fill "
                  style={{ marginTop: 'px', marginBottom: 'px', padding: '' }}
                >
                  <div className="wpb_column vc_column_container vc_col-sm-12">
                    <div className="vc_column-inner">
                      <div className="wpb_wrapper">
                        <div className="title-wrapper woodmart-title-color-default woodmart-title-style-simple woodmart-title-size-large woodmart-title-width-100 text-left">
                          <div className="liner-continer">
                            <span className="left-line" />
                            <h4 className="woodmart-title-container title  woodmart-font-weight-">
                              <span className="lang1">Featured Products</span>
                              <span className="lang2">Featured Products</span>
                            </h4>
                            <span className="right-line" />
                          </div>
                          <style dangerouslySetInnerHTML={{ __html: '' }} />
                        </div>
                        <div className="woodmart-products-element">
                          <div
                            className="products elements-grid row woodmart-products-holder woodmart-spacing-20 products-spacing-20 grid-columns-4 pagination-none"
                            data-paged={1}
                          >
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on product-type-variable  product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={1}
                              data-id={9151025020972}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/vestibulum-purus-ipsum"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/furniture2_250x250_crop_center.jpg?v=1525927036"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Vestibulum purus ipsum"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/vestibulum-purus-ipsum">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/furniture2_4_250x250_crop_center.jpg?v=1525927036"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="vestibulum-purus-ipsum"
                                      data-product-title="Vestibulum purus ipsum"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="vestibulum-purus-ipsum"
                                      data-product-title="Vestibulum purus ipsum"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/vestibulum-purus-ipsum?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                                <div className="quick-shop-wrapper">
                                  <div className="quick-shop-close">
                                    <span data-translate="collections.general.close">
                                      Close
                                    </span>
                                  </div>
                                  <div className="quick-shop-form" />
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/vestibulum-purus-ipsum">
                                  <span className="lang1">
                                    Vestibulum purus ipsum
                                  </span>
                                  <span className="lang2">
                                    Vestibulum purus ipsum
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$45.00"
                                        >
                                          $45.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        href="/products/vestibulum-purus-ipsum?view=json"
                                        className="button product_type_variable add_to_cart_button add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.select_options">
                                          Select Options
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid">
                                  <div
                                    className="swatch-on-grid woodmart-tooltip swatch-has-image swatch-size-default"
                                    style={{
                                      backgroundImage:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/black.png?v=1354171996728621107)',
                                    }}
                                    data-image-src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/furniture2_250x250_crop_center.jpg?v=1525927036"
                                    data-original-title
                                    title
                                  >
                                    Black
                                  </div>
                                  <div
                                    className="swatch-on-grid woodmart-tooltip swatch-has-image swatch-size-default"
                                    style={{
                                      backgroundImage:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/brown.png?v=22443076234004432)',
                                    }}
                                    data-image-src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/furniture2_2_250x250_crop_center.jpg?v=1525927036"
                                    data-original-title
                                    title
                                  >
                                    Brown
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={2}
                              data-id={9151000838188}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/white-iphone-7-case"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product2_250x250_crop_center.jpg?v=1525926724"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="White iPhone 7 Case"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/white-iphone-7-case">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product2_2_250x250_crop_center.jpg?v=1525926724"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="white-iphone-7-case"
                                      data-product-title="White iPhone 7 Case"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="white-iphone-7-case"
                                      data-product-title="White iPhone 7 Case"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/white-iphone-7-case?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/white-iphone-7-case">
                                  <span className="lang1">
                                    White iPhone 7 Case
                                  </span>
                                  <span className="lang2">
                                    White iPhone 7 Case
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$429.00"
                                        >
                                          $429.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/white-iphone-7-case"
                                        data-pid={9151000838188}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div className="clearfix visible-xs-block" />
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={3}
                              data-id={9151017975852}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/dolor-ad-hac-torquent"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/clocks6_3_250x250_crop_center.jpg?v=1525926928"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Dolor ad hac torquent"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/dolor-ad-hac-torquent">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/product-clock-1-3_87ccdf82-fdfe-4192-9e9f-ea78bc2f6136_250x250_crop_center.jpg?v=1525926928"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="dolor-ad-hac-torquent"
                                      data-product-title="Dolor ad hac torquent"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="dolor-ad-hac-torquent"
                                      data-product-title="Dolor ad hac torquent"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/dolor-ad-hac-torquent?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/dolor-ad-hac-torquent">
                                  <span className="lang1">
                                    Dolor ad hac torquent
                                  </span>
                                  <span className="lang2">
                                    Dolor ad hac torquent
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$255.00"
                                        >
                                          $255.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/dolor-ad-hac-torquent"
                                        data-pid={9151017975852}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div className="clearfix clearfix visible-sm-block" />
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={4}
                              data-id={9151001231404}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/stylish-brand-watches"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product4_250x250_crop_center.jpg?v=1525926730"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Stylish Brand Watches"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/stylish-brand-watches">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product4_2_250x250_crop_center.jpg?v=1525926730"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="stylish-brand-watches"
                                      data-product-title="Stylish Brand Watches"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="stylish-brand-watches"
                                      data-product-title="Stylish Brand Watches"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/stylish-brand-watches?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/stylish-brand-watches">
                                  <span className="lang1">
                                    Stylish Brand Watches
                                  </span>
                                  <span className="lang2">
                                    Stylish Brand Watches
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/minimalism" title>
                                  Minimalism
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$159.00"
                                        >
                                          $159.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/stylish-brand-watches"
                                        data-pid={9151001231404}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div className="clearfix visible-xs-block" />
                            <div className="clearfix visible-md-block visible-lg-block" />
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={5}
                              data-id={9151001591852}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/slatea-suspendisse-2"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/light9_2_ba36b727-7877-48b2-bb3c-527d3d5eeb1a_250x250_crop_center.jpg?v=1525926736"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Slatea suspendisse"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/slatea-suspendisse-2">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/light9_1_f245ad94-8d3d-4fd4-9970-ba5436794e23_250x250_crop_center.jpg?v=1525926736"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="slatea-suspendisse-2"
                                      data-product-title="Slatea suspendisse"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="slatea-suspendisse-2"
                                      data-product-title="Slatea suspendisse"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/slatea-suspendisse-2?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/slatea-suspendisse-2">
                                  <span className="lang1">
                                    Slatea suspendisse
                                  </span>
                                  <span className="lang2">
                                    Slatea suspendisse
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/minimalism" title>
                                  Minimalism
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$429.00"
                                        >
                                          $429.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/slatea-suspendisse-2"
                                        data-pid={9151001591852}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={6}
                              data-id={9151000608812}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/shoes-white-collection"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product6_250x250_crop_center.jpg?v=1525926716"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Shoes White Collection"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/shoes-white-collection">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product6_2_250x250_crop_center.jpg?v=1525926716"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="shoes-white-collection"
                                      data-product-title="Shoes White Collection"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="shoes-white-collection"
                                      data-product-title="Shoes White Collection"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/shoes-white-collection?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/shoes-white-collection">
                                  <span className="lang1">
                                    Shoes White Collection
                                  </span>
                                  <span className="lang2">
                                    Shoes White Collection
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$159.00"
                                        >
                                          $159.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/shoes-white-collection"
                                        data-pid={9151000608812}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div className="clearfix visible-xs-block" />
                            <div className="clearfix clearfix visible-sm-block" />
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={7}
                              data-id={9151001296940}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/minimalist-brown-back"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product1_1_250x250_crop_center.jpg?v=1525926733"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Minimalist Brown Back"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/minimalist-brown-back">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product1_250x250_crop_center.jpg?v=1525926733"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="minimalist-brown-back"
                                      data-product-title="Minimalist Brown Back"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="minimalist-brown-back"
                                      data-product-title="Minimalist Brown Back"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/minimalist-brown-back?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/minimalist-brown-back">
                                  <span className="lang1">
                                    Minimalist Brown Back
                                  </span>
                                  <span className="lang2">
                                    Minimalist Brown Back
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/minimalism" title>
                                  Minimalism
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$429.00"
                                        >
                                          $429.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/minimalist-brown-back"
                                        data-pid={9151001296940}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={8}
                              data-id={9151000281132}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/poster-minimalism-world"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product8_250x250_crop_center.jpg?v=1525926709"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Poster Minimalism World"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/poster-minimalism-world">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product8_2_250x250_crop_center.jpg?v=1525926709"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="poster-minimalism-world"
                                      data-product-title="Poster Minimalism World"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="poster-minimalism-world"
                                      data-product-title="Poster Minimalism World"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/poster-minimalism-world?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/poster-minimalism-world">
                                  <span className="lang1">
                                    Poster Minimalism World
                                  </span>
                                  <span className="lang2">
                                    Poster Minimalism World
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$159.00"
                                        >
                                          $159.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/poster-minimalism-world"
                                        data-pid={9151000281132}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div className="clearfix visible-xs-block" />
                            <div className="clearfix visible-md-block visible-lg-block" />
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={9}
                              data-id={9151000510508}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/organic-healthy-water"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product7_250x250_crop_center.jpg?v=1525926712"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Organic Healthy Water"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/organic-healthy-water">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product7_2_250x250_crop_center.jpg?v=1525926712"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="organic-healthy-water"
                                      data-product-title="Organic Healthy Water"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="organic-healthy-water"
                                      data-product-title="Organic Healthy Water"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/organic-healthy-water?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/organic-healthy-water">
                                  <span className="lang1">
                                    Organic Healthy Water
                                  </span>
                                  <span className="lang2">
                                    Organic Healthy Water
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$159.00"
                                        >
                                          $159.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/organic-healthy-water"
                                        data-pid={9151000510508}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div className="clearfix clearfix visible-sm-block" />
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={10}
                              data-id={9151000772652}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/black-money-holder-2017"
                                  className="product-image-link"
                                >
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product5_250x250_crop_center.jpg?v=1525926721"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Black Money Holder 2017"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/black-money-holder-2017">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product5_2_250x250_crop_center.jpg?v=1525926721"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="black-money-holder-2017"
                                      data-product-title="Black Money Holder 2017"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="black-money-holder-2017"
                                      data-product-title="Black Money Holder 2017"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/black-money-holder-2017?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/black-money-holder-2017">
                                  <span className="lang1">
                                    Black Money Holder 2017
                                  </span>
                                  <span className="lang2">
                                    Black Money Holder 2017
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$159.00"
                                        >
                                          $159.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        rel="nofollow"
                                        href="/collections/demo-minimalism/products/black-money-holder-2017"
                                        data-pid={9151000772652}
                                        className="button product_type_simple add_to_cart_button ajax_add_to_cart add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.add_to_cart">
                                          Add to Cart
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div className="clearfix visible-xs-block" />
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on   product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={11}
                              data-id={9151000936492}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/one-plus-3-premium"
                                  className="product-image-link"
                                >
                                  <div className="product-labels labels-rounded">
                                    <span
                                      className="out-of-stock product-label"
                                      data-translate="products.product.sold_out"
                                    >
                                      Sold Out
                                    </span>
                                  </div>
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product3_250x250_crop_center.jpg?v=1525926727"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="One Plus 3 Premium"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/one-plus-3-premium">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/minimalism-product3_2_250x250_crop_center.jpg?v=1525926727"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="one-plus-3-premium"
                                      data-product-title="One Plus 3 Premium"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="one-plus-3-premium"
                                      data-product-title="One Plus 3 Premium"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/one-plus-3-premium?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/one-plus-3-premium">
                                  <span className="lang1">
                                    One Plus 3 Premium
                                  </span>
                                  <span className="lang2">
                                    One Plus 3 Premium
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/clocks" title>
                                  Clocks
                                </a>
                                ,&nbsp;
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <span className="shopify-Price-amount amount">
                                        <span
                                          className="money"
                                          data-currency-usd="$569.00"
                                        >
                                          $569.00
                                        </span>
                                      </span>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        href="/collections/demo-minimalism/products/one-plus-3-premium"
                                        className="button product_type_variable add_to_cart_button add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.sold_out">
                                          Sold Out
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid"></div>
                              </div>
                            </div>
                            <div
                              className="product-grid-item product has-stars purchasable woodmart-hover-alt quick-shop-on product-type-variable  product-with-swatches col-xs-6 col-sm-4 col-md-3 type-product status-publish has-post-thumbnail first instock sale hover-ready hover-width-big"
                              data-loop={12}
                              data-id={9151015125036}
                            >
                              <div className="product-element-top">
                                <a
                                  href="/collections/demo-minimalism/products/hanging-lamp-berlingo"
                                  className="product-image-link"
                                >
                                  <div className="product-labels labels-rounded">
                                    <span className="onsale product-label">
                                      -32%
                                    </span>
                                  </div>
                                  <img
                                    src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/product-light-5_250x250_crop_center.jpg?v=1525926891"
                                    className="attachment-shop_catalog size-shop_catalog"
                                    alt="Hanging lamp berlingo"
                                  />
                                </a>
                                <div className="hover-img">
                                  <a href="/collections/demo-minimalism/products/hanging-lamp-berlingo">
                                    <img
                                      src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/product-light-5-2_250x250_crop_center.jpg?v=1525926891"
                                      className="attachment-shop_catalog size-shop_catalog"
                                      alt=""
                                    />
                                  </a>
                                </div>
                                <div className="woodmart-buttons">
                                  <div className="wrap-wishlist-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="hanging-lamp-berlingo"
                                      data-product-title="Hanging lamp berlingo"
                                      className="add_to_wishlist woodmart-tltp"
                                      title="Add to wishlist"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to wishlist
                                      </span>
                                      <span data-translate="wish_list.general.add_to_wishlist">
                                        Add to wishlist
                                      </span>
                                    </a>
                                    <div className="clear" />
                                  </div>
                                  <div className="clear" />
                                  <div className="product-compare-button">
                                    <a
                                      href="javascript:;"
                                      data-product-handle="hanging-lamp-berlingo"
                                      data-product-title="Hanging lamp berlingo"
                                      className="compare button woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Add to compare
                                      </span>
                                      <span data-translate="compare_list.general.add_to_compare">
                                        Add to compare
                                      </span>
                                    </a>
                                  </div>
                                  <div className="quick-view">
                                    <a
                                      href="/products/hanging-lamp-berlingo?view=quickview"
                                      className="quickview-icon quickview open-quick-view woodmart-tltp"
                                    >
                                      <span className="woodmart-tooltip-label">
                                        Quick View
                                      </span>
                                      <span data-translate="collections.general.quickview">
                                        Quick View
                                      </span>
                                    </a>
                                  </div>
                                </div>
                                <div className="quick-shop-wrapper">
                                  <div className="quick-shop-close">
                                    <span data-translate="collections.general.close">
                                      Close
                                    </span>
                                  </div>
                                  <div className="quick-shop-form" />
                                </div>
                              </div>
                              <h3 className="product-title">
                                <a href="/collections/demo-minimalism/products/hanging-lamp-berlingo">
                                  <span className="lang1">
                                    Hanging lamp berlingo
                                  </span>
                                  <span className="lang2">
                                    Hanging lamp berlingo
                                  </span>
                                </a>
                              </h3>
                              <div className="woodmart-product-cats">
                                <a href="/collections/demo-minimalism" title>
                                  Demo minimalism
                                </a>
                                ,&nbsp;
                                <a href="/collections/furniture" title>
                                  Furniture
                                </a>
                                ,&nbsp;
                                <a href="/collections/lighting" title>
                                  Lighting
                                </a>
                                ,&nbsp;
                                <a href="/collections/shop" title>
                                  Shop
                                </a>
                              </div>
                              <div className="wrap-price">
                                <div className="swap-wrapp">
                                  <div className="swap-elements">
                                    <span className="price">
                                      <del>
                                        <span className="shopify-Price-amount amount">
                                          <span
                                            className="money"
                                            data-currency-usd="$250.00"
                                          >
                                            $250.00
                                          </span>
                                        </span>
                                      </del>
                                      <ins>
                                        <span className="shopify-Price-amount amount">
                                          <span
                                            className="money"
                                            data-currency-usd="$169.00"
                                          >
                                            $169.00
                                          </span>
                                        </span>
                                      </ins>
                                    </span>
                                    <div className="btn-add-swap">
                                      <a
                                        href="/products/hanging-lamp-berlingo?view=json"
                                        className="button product_type_variable add_to_cart_button add-to-cart-loop"
                                      >
                                        <span data-translate="products.product.select_options">
                                          Select Options
                                        </span>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                                <div className="swatches-on-grid">
                                  <div
                                    className="swatch-on-grid woodmart-tooltip swatch-has-image swatch-size-default"
                                    style={{
                                      backgroundImage:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/black.png?v=1354171996728621107)',
                                    }}
                                    data-image-src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/product-light-5-2_250x250_crop_center.jpg?v=1525926891"
                                    data-original-title
                                    title
                                  >
                                    Black
                                  </div>
                                  <div
                                    className="swatch-on-grid woodmart-tooltip swatch-has-image swatch-size-default"
                                    style={{
                                      backgroundImage:
                                        'url(//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/gray.png?v=13901624850455997578)',
                                    }}
                                    data-image-src="//cdn.shopify.com/s/files/1/0002/7410/4364/products/product-light-5_250x250_crop_center.jpg?v=1525926891"
                                    data-original-title
                                    title
                                  >
                                    Gray
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="clearfix visible-xs-block" />
                            <div className="clearfix clearfix visible-sm-block" />
                            <div className="clearfix visible-md-block visible-lg-block" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <style
                  dangerouslySetInnerHTML={{
                    __html:
                      '\n    .vc_custom_1517802175241 { \n      border-top-width: 0px !important;background-color: transparent;\n    }',
                  }}
                />
              </div>
            </div>
            {/* END content_for_index */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Body;
