import Image from 'next/image';
import React from 'react';
import styles from './styles.module.css';
import logo from 'images/wood-logo-dark.svg';

function SiteLogo() {
  return (
      <div className={styles.siteLogo}>
        <a href="#">
          <Image className="logo" src={logo} height="30px" width="209px" alt="logo" />
        </a>
      </div>
  );
}

export default SiteLogo;
