import React, { useRef } from 'react';
import { mobileNavTabsActions } from 'store/features/MobileNavTabs/slice';
import { useAppDispatch } from 'store/hooks';
import styles from './styles.module.css';

function MainMenu() {
  const dispatch = useAppDispatch();

  const handleNavTabs = () => {
    dispatch(mobileNavTabsActions.setOpening(true));
  };

  return (
    <>
      <div className={styles.headerLeftSide}>
        <div
          className={`${styles.burgerIcon} ${styles.mobileNavIcon}`}
          onClick={handleNavTabs}
        >
          <span className={styles.burger}></span>
          <span className={styles.burgerLabel}>Menu</span>
        </div>
      </div>

      <div className={`${styles.mainNav} ${styles.menuLeft}`}>
        <ul className={styles.menu}>
          <li className={styles.menuItem}>
            <a href="#">
              <span>Home</span>
            </a>
          </li>

          <li className={styles.menuItem}>
            <a href="#">
              <span>Product</span>
            </a>
          </li>

          <li className={styles.menuItem}>
            <a href="#">
              <span>About</span>
            </a>
          </li>
        </ul>
      </div>
    </>
  );
}

export default MainMenu;
