import React from 'react';
import MainMenu from './MainMenu';
import RightColumn from './RightColumn';
import SiteLogo from './SiteLogo';
import styles from './styles.module.css';
function Header() {
  return (
    <>
      <div className={styles.headerSpacing} style={{ height: '125px' }}></div>
      <header
        className={`${styles.mainHeader} ${styles.headerShop} ${styles.headerMobileCenter} ${styles.headerStickyReal}`}
      >
        <div className={styles.container}>
          <div className={styles.wrapHeader} style={{ minHeight: '85px' }}>
            <MainMenu />
            <SiteLogo />
            <RightColumn />
          </div>
        </div>
      </header>
    </>
  );
}

export default Header;
