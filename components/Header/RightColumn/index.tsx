import React, { useState } from 'react';
import styles from './styles.module.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSearch,
  faTimes,
  faShoppingCart,
} from '@fortawesome/free-solid-svg-icons';
import { faHeart } from '@fortawesome/free-regular-svg-icons';
import { useAppDispatch } from 'store/hooks';
import { cartActions } from 'store/features/Cart/slice';

function RightColumn() {
  const dispatch = useAppDispatch();
  const [isOpenSearchForm, setOpenSearchForm] = useState(false);

  const openFormSearch = () => {
    setOpenSearchForm(!isOpenSearchForm);
  };

  const handleCart = () => {
    dispatch(cartActions.setOpening(true));
  }
  return (
    <div className={styles.rightColumn}>
      <div className={styles.headerLinks}>
        <ul>
          <li>
            <a href="#">
              <span>Login / Register</span>
            </a>
          </li>
        </ul>
      </div>

      <div className={styles.searchButton}>
        <a onClick={openFormSearch}>
          <FontAwesomeIcon
            icon={isOpenSearchForm ? faTimes : faSearch}
            size="lg"
          />
        </a>
        <div
          className={`${styles.searchWrapper} ${
            isOpenSearchForm
              ? styles.openSearchWrapper
              : styles.closeSearchWrapper
          }`}
          style={{ top: '85px' }}
        >
          <div className={styles.searchInner}>
            <span className={styles.closeSearch}>close</span>
            <form className={styles.searchform}>
              <div>
                <input
                  type="text"
                  placeholder="Search for products"
                  style={{ paddingRight: '65px' }}
                />
                <button type="submit" className={styles.searchsubmit}>
                  Search
                </button>
              </div>
            </form>

            <div className={styles.searchInfoText}>
              <span>Start typing to see products you are looking for.</span>
            </div>
          </div>
        </div>
      </div>

      <div className={styles.wishlistInfoWidget}>
        <a href="#">
          <span className={styles.wishlistInfoWrap}>
            <FontAwesomeIcon icon={faHeart} size="lg" />
            <span className={styles.count}>1</span>
          </span>
        </a>
      </div>

      <div className={styles.shoppingCart} onClick={handleCart}>
        <a href="#">
          <span className={styles.cartTotals}>
            <FontAwesomeIcon icon={faShoppingCart} size="lg" />
            <span className={styles.cartNumber}>
              &nbsp;0<span className={styles.numberItems}> item(s)</span>
            </span>
            <span className={styles.divider}> / </span>
            <span className={styles.money}>$0.00</span>
          </span>
        </a>
      </div>
    </div>
  );
}

export default RightColumn;
