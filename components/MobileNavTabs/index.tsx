import React, { useEffect, useState } from 'react';
import styles from './styles.module.css';
import { library } from '@fortawesome/fontawesome-svg-core';

import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from '@fortawesome/free-regular-svg-icons';
import { useAppSelector } from 'store/hooks';
import { selectOpening } from 'store/features/MobileNavTabs/slice';

function MobileNavTabs() {
  const selectOpeningNav = useAppSelector(selectOpening);
  const [isMenu, setMenu] = useState(true);
  const [isCategories, setCategories] = useState(false);

  useEffect(() => {});

  const handleMenu = () => {
    if (!isMenu) setMenu(true);
    if (isCategories) setCategories(false);
  };

  const handleCategories = () => {
    if (!isCategories) setCategories(true);
    if (isMenu) setMenu(false);
  };

  return (
    <div
      className={`${styles.mobileNav} ${
        selectOpeningNav ? styles.openMobileNav : styles.closeMobileNav
      }`}
    >
      <form className={styles.searchForm}>
        <input type="text" placeholder="Search for products" />
        <Button className={styles.searchSubmit}>
          <FontAwesomeIcon icon={faSearch} />
        </Button>
      </form>

      <div className={styles.mobileNavTabs}>
        <ul>
          <li
            onClick={handleMenu}
            className={`${styles.mobileTabTitle} ${styles.mobilePagesTitle} ${
              isMenu && styles.active
            }`}
          >
            <span>Menu</span>
          </li>
          <li
            onClick={handleCategories}
            className={`${styles.mobileTabTitle} ${
              styles.mobileCategoriesTitle
            } ${isCategories && styles.active}`}
          >
            <span>Categories</span>
          </li>
        </ul>
      </div>

      {/* Categories */}
      <div
        className={`${styles.mobileMenuTab} ${isCategories && styles.active}`}
      >
        <ul className={styles.siteMobileMenu}>
          <li>
            <a href="#">
              <span>Furniture</span>
            </a>
          </li>
          <li>
            <a href="#">
              <span>Cooking</span>
            </a>
          </li>
          <li>
            <a href="#">
              <span>Accessories</span>
            </a>
          </li>
          <li>
            <a href="#">
              <span>Fashion</span>
            </a>
          </li>
        </ul>
      </div>

      {/* Menu Tab*/}
      <div className={`${styles.mobileMenuTab} ${isMenu && styles.active}`}>
        <ul className={styles.siteMobileMenu}>
          <li>
            <div className={styles.wishlistInfoWidget}>
              <a href="#">
                <span className={styles.wishlistInfoWrap}>
                  <FontAwesomeIcon icon={faHeart} />
                  <span> Wishlist</span>
                </span>
              </a>
            </div>
          </li>

          <li>
            <a href="#">
              <FontAwesomeIcon icon={faSignInAlt} />
              <span> Login / Register</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default MobileNavTabs;
