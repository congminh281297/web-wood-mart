import React from 'react';
import styles from './styles.module.css';
import { library } from '@fortawesome/fontawesome-svg-core';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import { selectCartOpening } from 'store/features/Cart/slice';
function CartShop() {
  const dispatch = useAppDispatch();
  const selectOpenCart = useAppSelector(selectCartOpening);

  return (
    <>
      <div
        className={`${styles.cartWidgetSide} ${
          selectOpenCart && styles.cartOpened
        }`}
      >
        <div className={styles.widgetHeading}>
          <h3 className={styles.widgetTitle}>Shopping cart</h3>
          <a href="#" className={styles.widgetClose}>
            Close
          </a>
        </div>

        <div className={styles.widgetShoppingCart}>
          <div className={styles.widgetShoppingCartContent}>
            <div className={`${styles.scroll} ${styles.hasScrollbar}`}>
              <div className={styles.scrollContent}>
                <ul className={styles.productListWidget}>
                  <li className={styles.empty}>
                    <div style={{ marginBottom: '10px' }}>
                      <FontAwesomeIcon icon={faShoppingCart} size="5x" />
                    </div>
                    No products in the cart.
                  </li>
                  <p className={styles.returnToShop}>
                    <a className={styles.btnBackWard} href="#">
                      Return To Shop
                    </a>
                  </p>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CartShop;
