import React from 'react';
import { cartActions, selectCartOpening } from 'store/features/Cart/slice';
import {
  mobileNavTabsActions,
  selectOpening,
} from 'store/features/MobileNavTabs/slice';
import { useAppDispatch, useAppSelector } from 'store/hooks';
import styles from './styles.module.css';
function CloseSide() {
  const selectOpeningNav = useAppSelector(selectOpening);
  const selectOpeningCart = useAppSelector(selectCartOpening);
  const dispatch = useAppDispatch();
  const handleCloseSide = () => {
    selectOpeningNav && dispatch(mobileNavTabsActions.setOpening(false));
    selectOpeningCart && dispatch(cartActions.setOpening(false));
  };
  return (
    <div
      onClick={handleCloseSide}
      className={`${styles.closeSide} ${
        (selectOpeningNav || selectOpeningCart) && styles.openSide
      }`}
    ></div>
  );
}

export default CloseSide;
