import { all } from "redux-saga/effects";
import cartSaga from "./features/Cart/saga";
import moblieNavTabsSaga from "./features/MobileNavTabs/saga";

export function* rootSaga() {
  yield all([moblieNavTabsSaga(), cartSaga()]);
}
