import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';
export interface cartState {
  opening: boolean;
}
const initialState: cartState = {
  opening: false,
};
const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
      setOpening(state, action: PayloadAction<boolean>){
          state.opening = action.payload;
      }
  },
});

// Actions
export const cartActions = cartSlice.actions;

// Selectors
export const selectCartOpening = (state: RootState) => state.cart.opening;

// Reducer
const cartReducer = cartSlice.reducer;
export default cartReducer;