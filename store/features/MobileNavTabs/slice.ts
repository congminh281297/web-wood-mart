import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from 'store';
export interface MobileNavTabsState {
  opening: boolean;
}
const initialState: MobileNavTabsState = {
  opening: false,
};
const mobileNavTabsSlice = createSlice({
  name: 'mobileNavTabs',
  initialState,
  reducers: {
      setOpening(state, action: PayloadAction<boolean>){
          state.opening = action.payload;
      }
  },
});

// Actions
export const mobileNavTabsActions = mobileNavTabsSlice.actions;

// Selectors
export const selectOpening = (state: RootState) => state.mobileNavTabs.opening;

// Reducer
const mobileNavTabsReducer = mobileNavTabsSlice.reducer;
export default mobileNavTabsReducer;