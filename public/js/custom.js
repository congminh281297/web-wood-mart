var woodmart_settings = {};
woodmart_settings.enableCurrency = true;
woodmart_settings.ajax_cart_enable = true;
woodmart_settings.header_banner_version = '1';
woodmart_settings.header_banner_close_btn = false;
woodmart_settings.header_banner_enabled = false;
woodmart_settings.product_media = {};
woodmart_settings.cart_data = {};
woodmart_settings.cart_data.shoping_cart_action = 'widget';
woodmart_settings.product_data = {};
woodmart_settings.wishlist_data = {};
woodmart_settings.compare_data = {};
woodmart_settings.search = {};
woodmart_settings.search.ajax_search = true;
woodmart_settings.search.search_by_collection = true;
woodmart_settings.newsletter_enable = '1';
woodmart_settings.newsletter_show_after = 'time';
woodmart_settings.newsletter_hidden_mobile = false;
woodmart_settings.newsletter_time_delay = 3000;
woodmart_settings.newsletter_scroll_delay = 700;
woodmart_settings.newsletter_on_page = '0';

woodmart_settings.product_data.product_swatch_setting = 1;
if (multi_language && translator.isLang2()) {
  woodmart_settings.product_data.share_fb =
    window.lang2.products.product.share_fb;
  woodmart_settings.product_data.tweet =
    window.lang2.products.product.share_tweet;
  woodmart_settings.product_data.pin_it =
    window.lang2.products.product.share_pin_it;
  woodmart_settings.product_data.download_image =
    window.lang2.products.product.share_download;
} else {
  woodmart_settings.product_data.share_fb = 'Facebook';
  woodmart_settings.product_data.tweet = 'Twitter';
  woodmart_settings.product_data.pin_it = 'Pinit';
  woodmart_settings.product_data.download_image = 'Download';
}
if (multi_language && translator.isLang2()) {
  woodmart_settings.product_data.in_stock =
    window.lang2.products.product.in_stock;
  woodmart_settings.product_data.out_of_stock =
    window.lang2.products.product.out_of_stock;
  woodmart_settings.product_data.add_to_cart =
    window.lang2.products.product.add_to_cart;
  woodmart_settings.product_data.sold_out =
    window.lang2.products.product.sold_out;
  woodmart_settings.product_data.sku_na = window.lang2.products.product.sku_na;
  woodmart_settings.cart_data.totalNumb = window.lang2.cart.header.total_numb;
  woodmart_settings.cart_data.buttonViewCart =
    window.lang2.cart.header.view_cart;
  woodmart_settings.cart_data.continueShop =
    window.lang2.cart.header.continue_shopping;
  woodmart_settings.cart_data.addedCart = window.lang2.cart.header.added_cart;
  window.inventory_text = {
    in_stock: window.lang2.products.product.in_stock,
    many_in_stock: window.lang2.products.product.many_in_stock,
    out_of_stock: window.lang2.products.product.out_of_stock,
    add_to_cart: window.lang2.products.product.add_to_cart,
    sold_out: window.lang2.products.product.sold_out,
  };
  window.date_text = {
    year_text: window.lang2.general.date.year_text,
    month_text: window.lang2.general.date.month_text,
    week_text: window.lang2.general.date.week_text,
    day_text: window.lang2.general.date.day_text,
    year_singular_text: window.lang2.general.date.year_singular_text,
    month_singular_text: window.lang2.general.date.month_singular_text,
    week_singular_text: window.lang2.general.date.week_singular_text,
    day_singular_text: window.lang2.general.date.day_singular_text,
    hour_text: window.lang2.general.date.hour_text,
    min_text: window.lang2.general.date.min_text,
    sec_text: window.lang2.general.date.sec_text,
    hour_singular_text: window.lang2.general.date.hour_singular_text,
    min_singular_text: window.lang2.general.date.min_singular_text,
    sec_singular_text: window.lang2.general.date.sec_singular_text,
  };
  woodmart_settings.wishlist_data.wishlist =
    window.lang2.wish_list.general.wishlist;
  woodmart_settings.wishlist_data.product =
    window.lang2.wish_list.general.product;
  woodmart_settings.wishlist_data.quantity =
    window.lang2.wish_list.general.quantity;
  woodmart_settings.wishlist_data.options =
    window.lang2.wish_list.general.options;
  woodmart_settings.wishlist_data.remove =
    window.lang2.wish_list.general.remove;
  woodmart_settings.wishlist_data.no_item =
    window.lang2.wish_list.general.no_item;
  woodmart_settings.wishlist_data.item_exist =
    window.lang2.wish_list.general.item_exist;
  woodmart_settings.wishlist_data.item_added =
    window.lang2.wish_list.general.item_added;
  woodmart_settings.compare_data.compare =
    window.lang2.compare_list.general.wishlist;
  woodmart_settings.compare_data.product =
    window.lang2.compare_list.general.product;
  woodmart_settings.compare_data.quantity =
    window.lang2.compare_list.general.quantity;
  woodmart_settings.compare_data.options =
    window.lang2.compare_list.general.options;
  woodmart_settings.compare_data.remove =
    window.lang2.compare_list.general.remove;
  woodmart_settings.compare_data.no_item =
    window.lang2.compare_list.general.no_item;
  woodmart_settings.compare_data.item_exist =
    window.lang2.compare_list.general.item_exist;
  woodmart_settings.compare_data.item_added =
    window.lang2.compare_list.general.item_added;
  woodmart_settings.login_btn_text = window.lang2.customer.login.submit;
  woodmart_settings.register_btn_text = window.lang2.customer.register.submit;
} else {
  woodmart_settings.product_data.in_stock = 'In Stock';
  woodmart_settings.product_data.out_of_stock = 'Out Of Stock';
  woodmart_settings.product_data.add_to_cart = 'Add to Cart';
  woodmart_settings.product_data.sold_out = 'Sold Out';
  woodmart_settings.product_data.sku_na = 'N/A';
  woodmart_settings.cart_data.totalNumb = 'item(s)';
  woodmart_settings.cart_data.buttonViewCart = 'View cart';
  woodmart_settings.cart_data.continueShop = 'Continue shopping';
  woodmart_settings.cart_data.addedCart =
    'Product was successfully added to your cart.';
  window.inventory_text = {
    in_stock: 'In Stock',
    many_in_stock: 'Many In Stock',
    out_of_stock: 'Out Of Stock',
    add_to_cart: 'Add to Cart',
    sold_out: 'Sold Out',
  };

  window.date_text = {
    year_text: 'years',
    month_text: 'months',
    week_text: 'weeks',
    day_text: 'days',
    year_singular_text: 'year',
    month_singular_text: 'month',
    week_singular_text: 'week',
    day_singular_text: 'day',
    hour_text: 'Hr',
    min_text: 'Min',
    sec_text: 'Sc',
    hour_singular_text: 'Hour',
    min_singular_text: 'Min',
    sec_singular_text: 'Sec',
  };
  woodmart_settings.wishlist_data.wishlist = 'Wishlist';
  woodmart_settings.wishlist_data.product = 'Product';
  woodmart_settings.wishlist_data.quantity = 'Quantity';
  woodmart_settings.wishlist_data.options = 'Options';
  woodmart_settings.wishlist_data.remove = 'has removed from wishlist';
  woodmart_settings.wishlist_data.no_item = 'There is no items in wishlist box';
  woodmart_settings.wishlist_data.item_exist = 'is exist in wishlist';
  woodmart_settings.wishlist_data.item_added =
    'has added to wishlist successful';
  woodmart_settings.compare_data.compare = 'Comparing box';
  woodmart_settings.compare_data.product = 'Product';
  woodmart_settings.compare_data.quantity = 'Quantity';
  woodmart_settings.compare_data.options = 'Options';
  woodmart_settings.compare_data.remove = 'has removed from comparing box';
  woodmart_settings.compare_data.no_item = 'There is no items in comparing box';
  woodmart_settings.compare_data.item_exist = 'is exist in compare';
  woodmart_settings.compare_data.item_added =
    'has added to comparing box successful';
  woodmart_settings.login_btn_text = 'Log in';
  woodmart_settings.register_btn_text = 'Register';
}
