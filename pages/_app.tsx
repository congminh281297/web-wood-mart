import { useEffect } from 'react';
import { config } from '@fortawesome/fontawesome-svg-core';
import '@fortawesome/fontawesome-svg-core/styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import withReduxSaga from 'next-redux-saga';
import { createWrapper } from 'next-redux-wrapper';
import type { AppProps } from 'next/app';
import { store } from 'store';
import Header from 'components/Header1';
import Body from 'components/Body';
// import '../styles/globals.css';
config.autoAddCss = false;

function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    const next = document.getElementById('__next');
    if (next) {
      next.classList.add('website-wrapper');
    }
  }, []);

  return (
    <>
      <div className="header-spacing" style={{ height: 125 }}></div>

      <Header />
      <div className="clear"></div>
      <style
        type="text/css"
        dangerouslySetInnerHTML={{
          __html: ` .topbar-wrapp {
        background-color: #83b735;

        background-repeat: no-repeat;
        background-size: cover;
        background-attachment: default;
        background-position: center center;
      }
      .site-logo img {
        max-width: 209px;
      }
      .header-clone .site-logo img {
        max-width: 245px;
      }
      .main-header,
      .header-spacing {
        background-color: #ffffff;
      }
      .sticky-header.header-clone {
        background-color: #ffffff;
      }
      .woodmart-header-overlap:not(.template-product) .act-scroll {
        background-color: rgba(0, 0, 0, 0.9);
      }
      .main-header {
        border-bottom: 0px solid #ededed;

        background-repeat: no-repeat;
        background-size: cover;
        background-attachment: default;
        background-position: center center;
      }
      .navigation-wrap,
      .header-menu-top .navigation-wrap {
        background-color: ;
      }`,
        }}
      ></style>

      <Body />
      <Component {...pageProps} />
    </>
  );
}

MyApp.getInitialProps = async ({ Component, ctx }: any) => {
  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps({ ctx })
    : {};

  return { pageProps };
};

// export default MyApp;
const makeStore = () => store;
const wrapper = createWrapper(makeStore);
export default wrapper.withRedux(withReduxSaga(MyApp));
