/* eslint-disable @next/next/no-sync-scripts */
/* eslint-disable @next/next/no-css-tags */
import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
} from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html className="js svg flexbox csstransforms platform-Windows js_active vc_desktop vc_transform vc_transform">
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
          <link
            rel="shortcut icon"
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/favicon.ico?v=10450392825711304471"
            type="image/png"
          />
          <title>Woodmart Minimalism</title>
          {/* Helpers ================================================== */}
          {/* /snippets/social-meta-tags.liquid */}
          <meta property="og:type" content="website" />
          <meta property="og:title" content="Woodmart Minimalism" />
          <meta
            property="og:url"
            content="https://woodmart-minimalism.myshopify.com/"
          />
          <meta property="og:site_name" content="Woodmart Minimalism" />
          <meta name="twitter:card" content="summary" />
          <link
            rel="canonical"
            href="https://woodmart-minimalism.myshopify.com/"
          />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"
          />
          <meta name="theme-color" />
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/bootstrap.min.css?v=5822173120238085132"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          {/* CSS ==================================================+ */}
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/woodmart.css?v=1798883553917805505"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/font-awesome.min.css?v=11331228227774813278"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/owl.carousel.min.css?v=14542678224691900138"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/magnific-popup.css?v=9014834494843734138"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/styles.scss.css?v=17630601373619976559"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/photoswipe.css?v=16380328348233219803"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/animate.css?v=4656236633587963569"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          <link
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/color-config.scss.css?v=13555448760501229162"
            rel="stylesheet"
            type="text/css"
            media="all"
          />
          {/* Header hook for plugins ================================================== */}
          <meta
            id="shopify-digital-wallet"
            name="shopify-digital-wallet"
            content="/274104364/digital_wallets/dialog"
          />
          <link
            href="https://monorail-edge.shopifysvc.com"
            rel="dns-prefetch"
          />
          <link
            rel="stylesheet"
            media="screen"
            href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/compiled_assets/styles.css?0"
          />
          {/* /snippets/oldIE-js.liquid */}
          {/*[if lt IE 9]>


<link href="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/respond-proxy.html" id="respond-proxy" rel="respond-proxy" />
<link href="//woodmart-minimalism.myshopify.com/search?q=3fb97976919e75d473cad02e627bbe45" id="respond-redirect" rel="respond-redirect" />

<![endif]*/}
          <link
            href="//fonts.googleapis.com/css?family=Josefin Sans:300italic,400italic,500italic,600italic,700italic,800italic,700,300,600,800,400,500&subset=cyrillic-ext,greek-ext,latin,latin-ext,cyrillic,greek,vietnamese"
            rel="stylesheet"
            type="text/css"
          />
          <link
            href="//fonts.googleapis.com/css?family=Josefin Sans:300italic,400italic,500italic,600italic,700italic,800italic,700,300,600,800,400,500&subset=cyrillic-ext,greek-ext,latin,latin-ext,cyrillic,greek,vietnamese"
            rel="stylesheet"
            type="text/css"
          />
          <link
            href="//fonts.googleapis.com/css?family=Josefin Sans:300italic,400italic,500italic,600italic,700italic,800italic,700,300,600,800,400,500&subset=cyrillic-ext,greek-ext,latin,latin-ext,cyrillic,greek,vietnamese"
            rel="stylesheet"
            type="text/css"
          />
          <link
            href="//fonts.googleapis.com/css?family=Josefin Sans:300italic,400italic,500italic,600italic,700italic,800italic,700,300,600,800,400,500&subset=cyrillic-ext,greek-ext,latin,latin-ext,cyrillic,greek,vietnamese"
            rel="stylesheet"
            type="text/css"
          />
          <link
            href="//fonts.googleapis.com/css?family=Josefin Sans:300italic,400italic,500italic,600italic,700italic,800italic,700,300,600,800,400,500&subset=cyrillic-ext,greek-ext,latin,latin-ext,cyrillic,greek,vietnamese"
            rel="stylesheet"
            type="text/css"
          />
          <link
            href="//fonts.googleapis.com/css?family=Josefin Sans:300italic,400italic,500italic,600italic,700italic,800italic,700,300,600,800,400,500&subset=cyrillic-ext,greek-ext,latin,latin-ext,cyrillic,greek,vietnamese"
            rel="stylesheet"
            type="text/css"
          />
          <style
            dangerouslySetInnerHTML={{
              __html:
                '*[data-translate] {visibility:visible} .lang2 {display:none}',
            }}
          />
          <style
            dangerouslySetInnerHTML={{
              __html:
                '\n    \n    .single-product-content .product-options .selector-wrapper {\n      display: none;\n    }\n     \n    .font-text, body, .menu-label, .menu-item-register .create-account-link, .wpb-js-composer .vc_tta.vc_general.vc_tta-style-classic.vc_tta-accordion .vc_tta-panel-title, .widgetarea-mobile .widget_currency_sel_widget .widget-title, .widgetarea-mobile .widget_icl_lang_sel_widget .widget-title, .woodmart-hover-base .hover-content table th, .shopify-product-details__short-description table th { \n    font-weight: 300;\n} \n.slideshow-section .content_slideshow p.content1 { \n    font-weight: 300;\n    font-family: "Josefin Sans"; \n}\n.slideshow-section .content_slideshow .btn {\nfont-size: 14px;\n    line-height: 18px;\n    font-weight: 400;\n    padding: 15px 30px 11px;\n}\n.slideshow-section .owl-dots {\n    position: absolute;\n    top: 40%;\n    width: 30px;\n    right: -26px;\n    left: auto;\n    bottom: auto;\n}\n.slideshow-section .owl-dots .owl-dot {position:relative;}\n.slideshow-section .owl-dots .owl-dot:after {\ncontent: \'\';\n    position: absolute;\n    top: 15px;\n    right: 25px;\n    border-bottom: 2px solid #999;\n    height: 0;\n    width: 22px;\n    -webkit-transition: all .15s;\n    transition: all .15s;\n}\n.slideshow-section .owl-dots .owl-dot.active:after,.slideshow-section .owl-dots .owl-dot:hover:after {\nborder-bottom: 2px solid #000;\n    width: 50px;\n}\n\n.slideshow-section .owl-dots .owl-dot span {\n    border: none;\n    font-size: 16px;\n    background: none;\n    font-family: Josefin Sans;\n    font-weight: 600;\n    color: #999;\n    text-align: center;\n    transition: color .2s;\n}\n@media(min-width: 1024px){\n.slideshow-section .content_slideshow h4 {\nfont-size: 74px;\n    line-height: 78px;\n    font-weight: 400;\n}\n.slideshow-section .content_slideshow p.content1 {\nfont-size: 18px;\nline-height: 28px;\n}\n}\n@media(max-width:767px){\n.slideshow-section .slide-inner {\nheight: 270px;\n}\n.slideshow-section .content_slideshow .btn {\n    font-size: 11px;\n    line-height: 14px;\n    font-weight: 400;\n    padding: 10px 10px 7px;\n}\n.vc_custom_footer-subscribe-social .vc_column_container .border-inner {\nborder-right-width:0 !important;\n}\n}\n    \n  ',
            }}
          />
          <link
            href="https://cdn.shopify.com/shopifycloud/boomerang/shopify-boomerang-1.0.0.min.js"
            rel="preload"
            as="script"
          />
        </Head>
        <body
          id="woodmart-minimalism"
          className="page-template-default wrapper-full-width menu-style- woodmart-ajax-shop-on template-index  woodmart-top-bar-off  menu-style-bordered offcanvas-sidebar-mobile offcanvas-sidebar-tablet  woodmart-light btns-shop-light btns-accent-hover-light btns-accent-light btns-shop-hover-light btns-accent-3d  btns-shop-3d  enable-sticky-header sticky-header-real global-search-full-screen woodmart-header-shop header-full-width sticky-header-prepared document-ready"
        >
          <Main />
          <NextScript />

          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: ``,
            }}
          ></script>

          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: ``,
            }}
          ></script>

          <script src="/js/custom.js" type="text/javascript"></script>

          <script
            src="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/theme-scripts.js?v=7870559534518150295"
            type="text/javascript"
          ></script>
          <script
            src="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/jquery.zoom.min.js?v=13038990187238013756"
            type="text/javascript"
          ></script>
          <script
            src="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/photoswipe.min.js?v=18221617837540000111"
            type="text/javascript"
          ></script>

          <script
            src="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/option_selection.js?v=7263788734452933314"
            type="text/javascript"
          ></script>
          <script
            src="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/theme.min.js?v=10824849274166551912"
            type="text/javascript"
          ></script>
          <script
            src="//cdn.shopify.com/shopifycloud/shopify/assets/themes_support/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js"
            type="text/javascript"
          ></script>

          <script
            src="/services/javascripts/currencies.js"
            type="text/javascript"
          ></script>
          <script
            src="//cdn.shopify.com/s/files/1/0002/7410/4364/t/3/assets/jquery.currencies.min.js?v=5381246357056396404"
            type="text/javascript"
          ></script>

          {/* <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: ` var shopCurrency = '',
              defaultCurrency = '',
              cookieCurrency = '';
            currenciesCallback();
            function currenciesCallback() {
              Currency.format = 'money_format';

              shopCurrency = 'USD';

              Currency.money_with_currency_format[shopCurrency] = '${{
                amount,
              }} USD';
              Currency.money_format[shopCurrency] = '${{ amount }}';

              defaultCurrency = 'USD' || shopCurrency;

              cookieCurrency = Currency.cookie.read();

              jQuery('span.money span.money').each(function () {
                jQuery(this).parents('span.money').removeClass('money');
              });

              jQuery('span.money').each(function () {
                jQuery(this).attr('data-currency-USD', jQuery(this).html());
              });
              if (cookieCurrency == null) {
                if (shopCurrency !== defaultCurrency) {
                  Currency.convertAll(shopCurrency, defaultCurrency);
                } else {
                  Currency.currentCurrency = defaultCurrency;
                }
                Currency.cookie.write(defaultCurrency);
              } else if (cookieCurrency === shopCurrency) {
                Currency.currentCurrency = shopCurrency;
              } else {
                Currency.convertAll(shopCurrency, cookieCurrency);
                jQuery('.setting-currency li a').each(function (i, e) {
                  if (jQuery(e).data('currency') == cookieCurrency) {
                    jQuery(e).parent().addClass('selected');
                    jQuery('.woodmart-nav-link.current-currency span').html(
                      cookieCurrency
                    );
                  } else {
                    jQuery(e).parent().removeClass('selected');
                  }
                });
              }
              jQuery('.setting-currency li a').on('click', function (e) {
                e.preventDefault();
                jQuery('.setting-currency li').removeClass('selected');
                jQuery(this).parent().addClass('selected');
                var newCurrency = jQuery(this).attr('data-currency');
                jQuery('.woodmart-nav-link.current-currency span').html(newCurrency);
                Currency.convertAll(Currency.currentCurrency, newCurrency);
              });
            }
            function currenciesCallbackSpecial(id) {
              jQuery(id).each(function () {
                jQuery(this).attr('data-currency-USD', jQuery(this).html());
              });
              Currency.convertAll(
                shopCurrency,
                Currency.cookie.read(),
                id,
                'money_format'
              );
            }`,
            }}
          ></script> */}
        </body>
      </Html>
    );
  }
}

export default MyDocument;
